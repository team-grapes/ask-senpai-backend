import {
  QueryGetExamExpertGivenIdArgs,
  ExamExpert,
} from "../../generated/graphql";
import { ExamExpertModel } from "../../models/examExpert";

const getExamExpertGivenIdResolver = async (
  _: unknown,
  args: QueryGetExamExpertGivenIdArgs
): Promise<ExamExpert> => {
  const id = args.examExpertId;
  try {
    console.log(args);
    const existingExamExpert = await ExamExpertModel.findOne({
      _id: id,
    });
    if (!existingExamExpert) {
      throw new Error("No exam expert found with id = " + id);
    }

    console.log(existingExamExpert);
    const response: ExamExpert = {
      id: existingExamExpert.id,
      name: existingExamExpert.name,
      email: existingExamExpert.email,
      profilePic: existingExamExpert.profilePic,
      phone: existingExamExpert.phone,
      domainInfoMap: JSON.stringify(existingExamExpert.domainInfoMap),
      schedulingUrl: existingExamExpert.schedulingUrl,
      linkedinUrl: existingExamExpert.linkedinUrl,
      sessionPrice: existingExamExpert.sessionPrice,
    };
    return response;
  } catch (err) {
    throw err;
  }
};
export default getExamExpertGivenIdResolver;
