import { QueryGetStudentGivenIdArgs, Student } from "../../generated/graphql";
import { StudentModel } from "../../models/student";

const getStudentGivenIdResolver = async (
  _: unknown,
  args: QueryGetStudentGivenIdArgs
): Promise<Student> => {
  const id = args.studentId;
  try {
    console.log(args);
    const existingStudent = await StudentModel.findOne({
      _id: id,
    });
    if (!existingStudent) {
      throw new Error("No student found with id = " + id);
    }

    console.log(existingStudent);
    const response: Student = {
      id: existingStudent.id,
      name: existingStudent.name,
      email: existingStudent.email,
      profilePic: existingStudent.profilePic,
      phone: existingStudent.phone,
      domainInfoMap: JSON.stringify(existingStudent.domainInfoMap),
    };
    return response;
  } catch (err) {
    throw err;
  }
};
export default getStudentGivenIdResolver;
