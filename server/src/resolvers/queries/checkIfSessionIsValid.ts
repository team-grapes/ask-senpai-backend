import {
  QueryCheckIfSessionIsValidArgs,
  SessionValidityResponse
} from "../../generated/graphql";
import { SessionModel } from "../../models/session";
import { sessionBookingFutureTimeCushion } from "../../commonConstants";
import { StudentModel } from "../../models/student";
import { ExamExpertModel } from "../../models/examExpert";

const checkIfSessionIsValidResolver = async (
  _: unknown,
  args: QueryCheckIfSessionIsValidArgs
): Promise<SessionValidityResponse> => {
  const { examExpertId, studentId, startTime } = args.input;
  try {
    //console.log(args);
    if (startTime <= Date.now() / 1000) {
      return {
        isValid: false,
        message:
          "The time you have selected is from the past. Change it to a future time.",
      };
    } else if (
      startTime >=
      Date.now() / 1000 + sessionBookingFutureTimeCushion
    ) {
      return {
        isValid: false,
        message:
          "The time you have selected is more than a month away. It is too early to book a session.",
      };
    }
    const student = await StudentModel.findOne({
      _id: studentId,
    });
    if (!student) {
      return {
        isValid: false,
          message:
        "Could not find student with this id " + studentId,
      };
    }

    const examExpert = await ExamExpertModel.findOne({
      _id: examExpertId,
    });
    if (!examExpert) {
      return {
        isValid: false,
        message:
          "Could not find exam expert with this id " + examExpertId  ,
      };
    }

    const existingSession = await SessionModel.findOne({
      $or: [
        {
          startTimestamp: startTime,
          studentId: studentId,
        },
        {
          startTimestamp: startTime,
          studentId: examExpertId,
        },
      ],
    });
    if (existingSession) {
      return {
        isValid: false,
        message:
          "A session is already booked at this time. Change the appointment time and try again",
      };
    }
    return {
      isValid: true,
      message: "Session is available to book",
    };
  } catch (err) {
    throw err;
  }
};
export default checkIfSessionIsValidResolver;
