import {
  QueryGetSessionsGivenExamExpertIdArgs,
  Session,
} from "../../generated/graphql";
import { ISession, SessionModel } from "../../models/session";

const getSessionsGivenExamExpertIdResolver = async (
  _: unknown,
  args: QueryGetSessionsGivenExamExpertIdArgs
): Promise<Array<Session>> => {
  const exam_expert_id = args.examExpertId;
  try {
    //console.log(args);
    const sessions: Array<ISession> = await SessionModel.find({
      examExpertId: exam_expert_id,
    });
    //console.log(sessions);
    if (!sessions) {
      throw new Error(
        "No session found with exam expert id = " + exam_expert_id
      );
    }
    // console.log(sessions);
    const response: Array<Session> = sessions.map((sessionEntry) => ({
      id: sessionEntry._id,
      // @ts-ignore
      ...sessionEntry._doc,
    }));
    //console.log("Here is response", response)
    return response;
  } catch (err) {
    throw err;
  }
};
export default getSessionsGivenExamExpertIdResolver;
