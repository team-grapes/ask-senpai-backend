import {
  ExamExpert,
  FilterInput,
  FilterOperation,
  FilterType,
  QueryGetExamExpertsGivenFiltersArgs,
} from "../../generated/graphql";
import { ExamExpertModel } from "../../models/examExpert";

const getOperationMapper = (operation: FilterOperation): string => {
  if (operation == FilterOperation.Eql) return "$eq";
  if (operation == FilterOperation.Lte) return "$lte";
  return "$gte";
};
export const getExamExpertsGivenFiltersResolver = async (
  _: unknown,
  args: QueryGetExamExpertsGivenFiltersArgs
): Promise<Array<ExamExpert>> => {
  const filters: ReadonlyArray<FilterInput> = args.input;
  console.log(filters);
  let queryBuilder = {};
  for (let i = 0; i < filters.length; i += 1) {
    const filter = filters[i];
    const value =
      filter.filterType === FilterType.Number
        ? parseInt(filter.value)
        : filter.value;
    if (filter.field === "sessionPrice") {
      let existingFilters = {};
      if ("sessionPrice" in queryBuilder) {
        existingFilters = queryBuilder["sessionPrice"];
      }
      queryBuilder = {
        ...queryBuilder,
        [filter.field]: {
          ...existingFilters,
          [getOperationMapper(filter.operation)]: value,
        },
      };
    } else {
      queryBuilder = {
        ...queryBuilder,
        ["domainInfoMap." + filter.field]: {
          [getOperationMapper(filter.operation)]: value,
        },
      };
    }
  }
  console.log(queryBuilder);
  const examExperts = await ExamExpertModel.find(queryBuilder);
  const response: Array<ExamExpert> = examExperts.map(
    (examExpertEntry: any) => ({
      id: examExpertEntry._id,
      // @ts-ignore
      ...examExpertEntry._doc,
      domainInfoMap: JSON.stringify(examExpertEntry._doc.domainInfoMap),
    })
  );
  return response;
};
