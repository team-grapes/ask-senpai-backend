import {
  QueryGetSessionsGivenStudentIdArgs,
  Session,
} from "../../generated/graphql";
import { ISession, SessionModel } from "../../models/session";

const getSessionsGivenStudentIdResolver = async (
  _: unknown,
  args: QueryGetSessionsGivenStudentIdArgs
): Promise<Array<Session>> => {
  const student_id = args.studentId;
  try {
    const sessions: Array<ISession> = await SessionModel.find({
      studentId: student_id,
    });
    if (!sessions) {
      throw new Error("FAILED_SESSION_FETCH");
    }
    const response: Array<Session> = sessions.map((sessionEntry) => ({
      id: sessionEntry._id,
      //@ts-ignore
      ...sessionEntry._doc,
    }));
    return response;
  } catch (err) {
    throw err;
  }
};
export default getSessionsGivenStudentIdResolver;
