import {
  QueryGetSessionInfoGivenIdArgs,
  Session,
} from "../../generated/graphql";
import { SessionModel } from "../../models/session";

const getSessionInfoGivenIdResolver = async (
  _: unknown,
  args: QueryGetSessionInfoGivenIdArgs
): Promise<Session> => {
  const session_id = args.sessionId;
  try {
    //console.log(args);
    const session = await SessionModel.findOne({
      _id: session_id,
    });
    if (!session) {
      throw new Error("No session found with id = " + session_id);
    }
    return {
      // @ts-ignore
      ...session._doc,
      id: session._id,
    };
  } catch (err) {
    throw err;
  }
};
export default getSessionInfoGivenIdResolver;
