import { QueryResolvers } from "../../generated/graphql";

import getStudentGivenIdResolver from "./getStudentGivenId";
import getExamExpertGivenIdResolver from "./getExamExpertGivenId";
import getSessionsGivenStudentIdResolver from "./getSessionGivenStudent";
import getSessionsGivenExamExpertIdResolver from "./getSessionGivenExamExpert";
import getSessionInfoGivenIdResolver from "./getSessionGivenId";
import { getExamExpertsGivenFiltersResolver } from "./getExamExpertsGivenFiltersResolver";
import checkIfSessionIsValidResolver from "./checkIfSessionIsValid";

const queryResolvers: QueryResolvers = {
  getStudentGivenId: getStudentGivenIdResolver,
  getExamExpertGivenId: getExamExpertGivenIdResolver,
  getSessionsGivenStudentId: getSessionsGivenStudentIdResolver,
  getSessionsGivenExamExpertId: getSessionsGivenExamExpertIdResolver,
  getExamExpertsGivenFilters: getExamExpertsGivenFiltersResolver,
  getSessionInfoGivenId: getSessionInfoGivenIdResolver,
  checkIfSessionIsValid: checkIfSessionIsValidResolver,
};

export default queryResolvers;
