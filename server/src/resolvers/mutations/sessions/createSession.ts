import {
  CreateSessionResponse,
  RequireFields,
} from "../../../generated/graphql";
import { MutationCreateSessionArgs } from "../../../generated/graphql";
import { SessionModel } from "../../../models/session";
import { OrderModel } from "../../../models/order";
import { ExamExpertModel } from "../../../models/examExpert";
import { StudentModel } from "../../../models/student";
import sendBookingEmail from "../../../middleware/sessionBookingEmail";
require("dotenv").config();

const sendEmail = process.env.SEND_MAIL;

export const createSessionResolver = async (
  _: unknown,
  args: RequireFields<MutationCreateSessionArgs, "input">
): Promise<CreateSessionResponse> => {
  const { orderId } = args.input;
  try {
    const existingOrder = await OrderModel.findOne({
      _id: orderId,
    });
    if (!existingOrder) {
      throw new Error("No order found with given id");
    }

    const existingExamExpert = await ExamExpertModel.findOne({
      _id: existingOrder.examExpertId,
    });

    if (!existingExamExpert) {
      throw new Error("No Exam Expert found with given id");
    }
    const meetingURL = existingExamExpert.schedulingUrl;

    const existingStudent = await StudentModel.findOne({
      _id: existingOrder.studentId,
    });

    if (!existingStudent) {
      throw new Error("No student found with given id");
    }

    const Session = new SessionModel({
      examExpertId: existingOrder.examExpertId,
      studentId: existingOrder.studentId,
      agenda: existingOrder.agenda,
      videoURL: null,
      domain: existingOrder.domain,
      meetingURL: meetingURL,
      startTimestamp: existingOrder.startTimestamp,
      studentFeedback: null,
      examExpertReview: null,
      sessionPrice: existingOrder.sessionPrice,
    });
    const result = await Session.save();
    console.log(result);

    if (sendEmail) {
      sendBookingEmail(
        existingExamExpert.name,
        existingExamExpert.email,
        existingStudent.name,
        existingStudent.email,
        result.meetingURL,
        result.startTimestamp,
        result.agenda,
        result.domain
      );
    }
    const response: CreateSessionResponse = {
      session: {
        // @ts-ignore
        ...result._doc,
        id: result._id,
      },
      meetingLink: meetingURL,
    };
    return response;
  } catch (err) {
    throw err;
  }
};

export default createSessionResolver;
