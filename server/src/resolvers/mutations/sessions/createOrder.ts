import {
  MutationCreateOrderArgs,
  CreateOrderResponse,
  RequireFields,
} from "../../../generated/graphql";
import { OrderModel } from "../../../models/order";
import initiatePayment from "../../../utils/paymentUtils/payment";
import { StudentModel } from "../../../models/student";

const createOrderResolver = async (
  _: unknown,
  args: RequireFields<MutationCreateOrderArgs, "input">
): Promise<CreateOrderResponse> => {
  const {
    examExpertId,
    studentId,
    startTime,
    domain,
    sessionPrice,
    agenda,
  } = args.input;
  try {
    console.log(args);
    const student = await StudentModel.findOne({
      _id: studentId,
    });
    if (!student) {
      throw new Error("No Student found with given id");
    }

    const Order = new OrderModel({
      examExpertId: examExpertId,
      studentId: studentId,
      agenda: agenda,
      domain: domain,
      startTimestamp: startTime,
      sessionPrice: sessionPrice,
    });
    const result = await Order.save();
    console.log(result);
    const orderId = result.id;

    const paymentResponse: any = await initiatePayment(
      orderId,
      student.id,
      student.email,
      student.phone,
      sessionPrice
    );
    console.log(paymentResponse);
    if (paymentResponse) {
      const response: CreateOrderResponse = {
        orderId: orderId,
        paymentParams: paymentResponse,
      };
      console.log("Response is", paymentResponse);
      return response;
    } else {
      throw new Error("Payment error");
    }
  } catch (err) {
    throw err;
  }
};
export default createOrderResolver;
