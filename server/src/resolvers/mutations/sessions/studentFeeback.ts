import {
  MutationAddStudentFeedbackGivenSessionIdArgs,
  AddStudentFeedbackGivenSessionIdResponse,
} from "../../../generated/graphql";
import { SessionModel } from "../../../models/session";

const addStudentFeedbackGivenSessionIdResolver = async (
  _: unknown,
  args: MutationAddStudentFeedbackGivenSessionIdArgs
): Promise<AddStudentFeedbackGivenSessionIdResponse> => {
  const { sessionId, studentFeedback } = args.input;
  try {
    console.log(args);
    const existingSession = await SessionModel.findOne({
      _id: sessionId,
    });
    if (!existingSession) {
      throw new Error("No session found with id = " + sessionId);
    }
    // console.log(existingUser);
    existingSession.studentFeedback = studentFeedback;
    console.log(existingSession);
    const result: any = await existingSession.save();
    // console.log(result);
    const response: AddStudentFeedbackGivenSessionIdResponse = {
      session: {
        id: result.id,
        examExpertId: result.examExpertId,
        studentId: result.studentId,
        agenda: result.agenda,
        domain: result.domain,
        meetingURL: result.meetingURL,
        startTimestamp: result.startTimestamp,
        studentFeedback: result.studentFeedback,
        examExpertReview: result.examExpertReview,
        sessionPrice: result.sessionPrice,
      },
    };
    return response;
  } catch (err) {
    throw err;
  }
};
export default addStudentFeedbackGivenSessionIdResolver;
