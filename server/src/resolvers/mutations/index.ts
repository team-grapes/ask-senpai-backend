import { MutationResolvers } from "../../generated/graphql";
import applyForExamExpertResolver from "./examExpert/applicantApply";
import registerStudentResolver from "./student/registerStudent";
import editStudentInfoResolver from "./student/editStudentInfo";
import loginStudentResolver from "./student/loginStudent";
import editExamExpertInfoResolver from "./examExpert/editExamExpertInfo";
import loginExamExpertResolver from "./examExpert/loginExamExpert";
import addExamExpertReviewGivenSessionIdResolver from "./sessions/examExpertReview";
import addStudentFeedbackGivenSessionIdResolver from "./sessions/studentFeeback";
import createSessionResolver from "./sessions/createSession";
import registerExamExpertResolver from "./examExpert/registerExamExpert";
import createOrderResolver from "./sessions/createOrder";
import changePasswordResolver from "./password/changePassword";
import forgotPasswordResolver from "./password/forgotPassword";

const mutationResolvers: MutationResolvers = {
  loginStudent: loginStudentResolver,
  editStudentInfo: editStudentInfoResolver,
  registerStudent: registerStudentResolver,
  applyForExamExpert: applyForExamExpertResolver,
  editExamExpertInfo: editExamExpertInfoResolver,
  loginExamExpert: loginExamExpertResolver,
  addExamExpertReviewGivenSessionId: addExamExpertReviewGivenSessionIdResolver,
  addStudentFeedbackGivenSessionId: addStudentFeedbackGivenSessionIdResolver,
  createSession: createSessionResolver,
  registerExamExpert: registerExamExpertResolver,
  createOrder: createOrderResolver,
  changePassword: changePasswordResolver,
  forgotPassword : forgotPasswordResolver,
};
export default mutationResolvers;
