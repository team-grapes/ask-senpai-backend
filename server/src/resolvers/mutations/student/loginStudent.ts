import {
  MutationLoginStudentArgs,
  LoginStudentResponse,
} from "../../../generated/graphql";
import { StudentModel } from "../../../models/student";
import bcrypt from "bcrypt";
const jwt = require("jsonwebtoken");
require("dotenv").config();

const jwtSecretKey = process.env.JWT_SECRET_KEY;

const tokenExpirationTimeDuration = 3600; //time is in seconds

const loginStudentResolver = async (
  _: unknown,
  args: MutationLoginStudentArgs
): Promise<LoginStudentResponse> => {
  const { email, password } = args.input;
  console.log(jwtSecretKey);
  try {
    console.log(args);
    const user = await StudentModel.findOne({
      email: email,
    });
    if (!user) {
      throw new Error("User does not exist!");
    }
    const isEqual = await bcrypt.compare(password, user.password);
    if (!isEqual) {
      throw new Error("Password is incorrect!");
    }
    const token = jwt.sign(
      { userId: user.id, email: user.email },
      jwtSecretKey,
      {
        expiresIn: tokenExpirationTimeDuration,
      }
    );

    const response: LoginStudentResponse = {
      student: {
        id: user.id,
        name: user.name,
        email: user.email,
        profilePic: user.profilePic,
        phone: user.phone,
        domainInfoMap: JSON.stringify(user.domainInfoMap),
      },
      token: token,
      tokenExpiration: tokenExpirationTimeDuration,
    };
    return response;
  } catch (err) {
    throw err;
  }
};
export default loginStudentResolver;
