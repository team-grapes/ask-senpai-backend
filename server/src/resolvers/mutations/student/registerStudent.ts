import {
  MutationRegisterStudentArgs,
  RegisterStudentResponse,
} from "../../../generated/graphql";
import { IStudent, StudentModel } from "../../../models/student";
import bcrypt from "bcrypt";

const registerStudentResolver = async (
  _: unknown,
  args: MutationRegisterStudentArgs
): Promise<RegisterStudentResponse> => {
  const {
    email,
    name,
    phone,
    profilePic,
    domainInfoMap,
    password,
  } = args.input;
  try {
    console.log(args);
    const existingUser = await StudentModel.findOne({
      email: email,
    });
    if (existingUser) {
      throw new Error("Student exists already.");
    }
    const hashedPassword = await bcrypt.hash(password, 12);
    const student = new StudentModel({
      name: name,
      email: email,
      password: hashedPassword,
      phone: phone,
      profilePic: profilePic,
      domainInfoMap: JSON.parse(domainInfoMap),
    });
    const result: IStudent = await student.save();
    const response: RegisterStudentResponse = {
      student: {
        id: result.id,
        name: result.name,
        email: result.email,
        profilePic: result.profilePic,
        phone: result.phone,
        domainInfoMap: JSON.stringify(result.domainInfoMap),
      },
    };
    return response;
  } catch (err) {
    throw err;
  }
};
export default registerStudentResolver;
