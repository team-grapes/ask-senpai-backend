import {
  MutationEditStudentInfoArgs,
  EditStudentInfoResponse,
} from "../../../generated/graphql";
import { StudentModel } from "../../../models/student";

const editStudentInfoResolver = async (
  _: unknown,
  args: MutationEditStudentInfoArgs,
  { req }: any
): Promise<EditStudentInfoResponse> => {
  const { id, name, profilePic, phone, domainInfoMap } = args.input;
  console.log("This is request in editstudentInfo");
  console.log("Auth value is " + req.isAuth);
  // if (!req.isAuth) {
  //     throw new Error('Unauthenticated to edit profile!');
  // }
  //removed temporarily for Frontend testing
  try {
    console.log(args);
    const existingUser = await StudentModel.findOne({
      _id: id,
    });
    if (!existingUser) {
      throw new Error("No student found with id = " + id);
    }

    console.log(existingUser);
    existingUser.name = name;
    if(profilePic)  existingUser.profilePic = profilePic;
    existingUser.phone = phone;
    existingUser.domainInfoMap = JSON.parse(domainInfoMap);
    console.log(existingUser);
    const result: any = await existingUser.save();
    // console.log(result);
    const response: EditStudentInfoResponse = {
      student: {
        id: result.id,
        name: result.name,
        email: result.email,
        profilePic: result.profilePic,
        phone: result.phone,
        domainInfoMap: JSON.stringify(result.domainInfoMap),
      },
    };
    return response;
  } catch (err) {
    throw err;
  }
};
export default editStudentInfoResolver;
