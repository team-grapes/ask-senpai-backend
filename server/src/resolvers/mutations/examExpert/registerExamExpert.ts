import {
  RegisterExamExpertResponse,
  RequireFields,
} from "../../../generated/graphql";
import { MutationRegisterExamExpertArgs } from "../../../generated/graphql";
import { ExamExpertApplicantModel, IExamExpertApplicant } from "../../../models/examExpertApplicant";
import bcrypt from "bcrypt";
import { ExamExpertModel, IExamExpert } from "../../../models/examExpert";

const registerExamExpertResolver = async (
  _: unknown,
  args: RequireFields<MutationRegisterExamExpertArgs, "input">
): Promise<RegisterExamExpertResponse> => {
  const { applicantId, schedulingUrl, sessionPrice, password, linkedinUrl } = args.input;
  try {
    const existingApplicant: IExamExpertApplicant = await ExamExpertApplicantModel.findOne({
      _id: applicantId,
    });
    if (!existingApplicant) throw new Error("Applicant doesn't exists.");
    const existingUser = await ExamExpertModel.findOne({
      email: existingApplicant.email,
    });
    if (existingUser) throw new Error("Exam Expert already exists.");
    const hashedPassword = await bcrypt.hash(password, 12);

    const femaleRandomPic =
      "https://joeschmoe.io/api/v1/female/" +
      "grapes-" +
      existingApplicant.email;
    const maleRandomPic =
      "https://joeschmoe.io/api/v1/male/" + "grapes-" + existingApplicant.email;
    // @ts-ignore
    let applicantGender = existingApplicant.domainInfoMap.Gender;
    let profilePic =
      applicantGender == "male" ? maleRandomPic : femaleRandomPic;

    const examExpert: IExamExpert = new ExamExpertModel({
      name: existingApplicant.name,
      email: existingApplicant.email,
      password: hashedPassword,
      phone: existingApplicant.phone,
      profilePic: profilePic,
      sessionPrice: sessionPrice,
      schedulingUrl: schedulingUrl,
      linkedinUrl: linkedinUrl,
      domainInfoMap: existingApplicant.domainInfoMap,
    });
    const result: IExamExpert = await examExpert.save();
    const response: RegisterExamExpertResponse = {
      examExpert: {
        id: result.id,
        name: result.name,
        email: result.email,
        phone: result.phone,
        sessionPrice: result.sessionPrice,
        schedulingUrl: result.schedulingUrl,
        linkedinUrl: result.linkedinUrl,
        domainInfoMap: JSON.stringify(existingApplicant.domainInfoMap),
      },
    };
    return response;
  } catch (e) {
    throw new Error("Register Exam Expert Failed.");
  }
};
export default registerExamExpertResolver;
