import {
  MutationEditExamExpertInfoArgs,
  EditExamExpertInfoResponse,
} from "../../../generated/graphql";
import { ExamExpertModel } from "../../../models/examExpert";

const editExamExpertInfoResolver = async (
  _: unknown,
  args: MutationEditExamExpertInfoArgs,
  { req }: any
): Promise<EditExamExpertInfoResponse> => {
  const {
    id,
    name,
    profilePic,
    phone,
    domainInfoMap,
    sessionPrice,
  } = args.input;
  console.log("This is request in editexamExpertInfo");
  console.log("Auth value is " + req.isAuth);
  // if (!req.isAuth) {
  //   throw new Error("Unauthenticated to edit profile!");
  // }
  //removed for UI testing
  try {
    console.log(args);
    const existingExamExpert = await ExamExpertModel.findOne({
      _id: id,
    });
    if (!existingExamExpert) {
      throw new Error("No examExpert found with id = " + id);
    }
    // console.log(existingExamExpert);
    existingExamExpert.name = name;
    if(profilePic) existingExamExpert.profilePic = profilePic;
    existingExamExpert.phone = phone;
    existingExamExpert.domainInfoMap = JSON.parse(domainInfoMap);
    existingExamExpert.sessionPrice = sessionPrice;
    console.log(existingExamExpert);
    const result: any = await existingExamExpert.save();
    // console.log(result);
    const response: EditExamExpertInfoResponse = {
      examExpert: {
        id: result.id,
        name: result.name,
        email: result.email,
        profilePic: result.profilePic,
        phone: result.phone,
        domainInfoMap: JSON.stringify(result.domainInfoMap),
        schedulingUrl: result.schedulingUrl,
        linkedinUrl: result.linkedinUrl,
        sessionPrice: result.sessionPrice,
      },
    };
    return response;
  } catch (err) {
    throw err;
  }
};
export default editExamExpertInfoResolver;
