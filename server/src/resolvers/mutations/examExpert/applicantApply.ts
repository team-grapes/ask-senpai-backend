import {ApplyExamExpertResponse, RequireFields} from "../../../generated/graphql";
import {MutationApplyForExamExpertArgs} from "../../../generated/graphql"
import {ExamExpertApplicantModel} from "../../../models/examExpertApplicant";

const applyForExamExpertResolver = async (
    _: unknown,
    args: RequireFields<MutationApplyForExamExpertArgs, "input">
): Promise<ApplyExamExpertResponse> => {
    const {name, email, phone, domainInfoMap} =  args.input;
    try {
        console.log(args);
        const existingApplicant = await ExamExpertApplicantModel.findOne({
            email: email
        });
        if (existingApplicant) {
            throw new Error("Applicant exists already.");
        }
        const examExpertApplicant = new ExamExpertApplicantModel({
            name: name,
            email: email,
            phone: phone,
            domainInfoMap: JSON.parse(domainInfoMap)
        });
        const result = await examExpertApplicant.save();
        console.log(result);
        const response: ApplyExamExpertResponse = {
            applicantId: result.id
        };
        return response;
    } catch (err) {
        throw err;
    }
};

export default applyForExamExpertResolver;



