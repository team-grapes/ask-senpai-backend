import {
  MutationLoginExamExpertArgs,
  LoginExamExpertResponse,
} from "../../../generated/graphql";
import { ExamExpertModel } from "../../../models/examExpert";
import bcrypt from "bcrypt";
const jwt = require("jsonwebtoken");
require("dotenv").config();

const jwtSecretKey = process.env.JWT_SECRET_KEY;

const tokenExpirationTimeDuration = 3600; //time is in seconds

const loginExamExpertResolver = async (
  _: unknown,
  args: MutationLoginExamExpertArgs
): Promise<LoginExamExpertResponse> => {
  const { email, password } = args.input;
  try {
    //console.log(args);
    const result = await ExamExpertModel.findOne({
      email: email,
    });
    if (!result) {
      throw new Error("ExamExpert does not exist!");
    }
    //console.log(result)
    const isEqual = await bcrypt.compare(password, result.password);
    //console.log(isEqual)
    if (!isEqual) {
      throw new Error("Password is incorrect!");
    }
    console.log(jwtSecretKey);
    const token = jwt.sign(
      { userId: result.id, email: result.email },
      jwtSecretKey,
      {
        expiresIn: tokenExpirationTimeDuration,
      }
    );

    const response: LoginExamExpertResponse = {
      examExpert: {
        id: result.id,
        name: result.name,
        email: result.email,
        profilePic: result.profilePic,
        phone: result.phone,
        domainInfoMap: JSON.stringify(result.domainInfoMap),
        schedulingUrl: result.schedulingUrl,
        linkedinUrl: result.linkedinUrl,
        sessionPrice: result.sessionPrice,
      },
      token: token,
      tokenExpiration: tokenExpirationTimeDuration,
    };
    return response;
  } catch (err) {
    throw err;
  }
};
export default loginExamExpertResolver;
