import {
  MutationForgotPasswordArgs,
  ForgotPasswordResponse,
  RequireFields,
} from "../../../generated/graphql";
import { Chance } from "chance";
import { changePasswordUtil } from "./changePassword";
import { MailUtils } from "../../../utils/mailUtils";
let chance = new Chance();

const forgotPasswordResolver = async (
  _: unknown,
  args: RequireFields<MutationForgotPasswordArgs, "input">
): Promise<ForgotPasswordResponse> => {
  const {
    email,
    userType,
  } = args.input;
  try {
    console.log(args);
    var passwordString = chance.string({ length: 8, casing: 'upper', alpha: true, numeric: true });
    console.log(passwordString);
    const user = await changePasswordUtil(email, passwordString, userType);
    let mailService = new MailUtils();

    mailService.sendMail(
      email,
      "Forgot your password? We got you covered!",
      "Hi!" +
      ",\n" +
      "We have reset your new password to " +
      passwordString +
      " \n Login again with this password on platform and change the password once you are logged in again" +
      "If, for any technical reason, you are facing trouble in logging in, please let us know by giving us a call on 9534291132.\n" +
      "\n" +
      "Kind Regards,\n" +
      "The Grapes Team\n"
    );

    const response: ForgotPasswordResponse = {
      id: user.id,
      userType: userType,
    };
    return response;
  } catch (err) {
    throw err;
  }
};
export default forgotPasswordResolver;
