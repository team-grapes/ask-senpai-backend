import {
  MutationChangePasswordArgs,
  ChangePasswordResponse,
  RequireFields,
} from "../../../generated/graphql";
import { StudentModel } from "../../../models/student";
import {ExamExpertModel} from "../../../models/examExpert";
import bcrypt from "bcrypt";

export async function changePasswordUtil(email : string, newPassword : string, userType : string) {
  let user = null;
  const hashedPassword = await bcrypt.hash(newPassword, 12);
  if(userType == "EXAMEXPERT"){
    user = await ExamExpertModel.findOne({
      email: email,
    });
    if (!user) {
      throw new Error("No User found with given email " + email);
    }
  }
  else if(userType == "STUDENT"){
    user = await StudentModel.findOne({
      email: email,
    });
    if (!user) {
      throw new Error("No user found with given email " + email);
    }
  }
  else{
    throw new Error("Enum sent is incorrect got " + userType)
  }
  user.password = hashedPassword;
  const result: any = await user.save();
  return result;
}

const changePasswordResolver = async (
  _: unknown,
  args: RequireFields<MutationChangePasswordArgs, "input">
): Promise<ChangePasswordResponse> => {
  const {
    email,
    newPassword,
    userType,
  } = args.input;
  try {
    console.log(args);
    const user = await changePasswordUtil(email, newPassword, userType);
    const response: ChangePasswordResponse = {
      id: user.id,
      userType: userType,
    };
      return response;
  } catch (err) {
    throw err;
  }
};
export default changePasswordResolver;
