import {Session} from "../../generated/graphql";

export const sessionResolver = async (parent : { data: unknown }) : Promise<Session> => {
    return parent.data as Session;
}
