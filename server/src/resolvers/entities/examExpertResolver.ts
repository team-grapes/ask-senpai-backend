import {ExamExpert} from "../../generated/graphql";

export const examExpertResolver = async (parent : { data: unknown }) : Promise<ExamExpert> => {
    return parent.data as ExamExpert;
}
