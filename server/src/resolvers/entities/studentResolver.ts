import {Student} from "../../generated/graphql";

export const studentResolver = async (parent : { data: unknown }) : Promise<Student> => {
    return parent.data as Student;
}
