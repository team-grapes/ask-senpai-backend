import mutationResolvers from './mutations';
import queryResolvers from "../resolvers/queries";
import {studentResolver} from "./entities/studentResolver";
import {examExpertResolver} from "./entities/examExpertResolver";
import {sessionResolver} from "./entities/sessionResolver";

const resolvers = {
    Student: studentResolver,
    ExamExpert: examExpertResolver,
    Session: sessionResolver,
    Mutation: mutationResolvers,
    Query : queryResolvers
};

export default resolvers;
