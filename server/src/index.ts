import { ApolloServer } from "apollo-server-express";
import typeDefs from "./schema";
import resolvers from "../src/resolvers/";
import cors from "cors";
//import { resolvers } from "./resolvers";
import mongoose from "mongoose";
import express from "express";
import context from "../src/middleware/is-auth";
import { MailUtils } from "./utils/mailUtils";
require("dotenv").config();

// @ts-ignore
const server = new ApolloServer({ typeDefs, resolvers, context, mocks: false });
const app = express();
const bodyParser = require("body-parser");
const MONGO_URL = `mongodb+srv://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@cluster0.ei7rf.mongodb.net/${process.env.MONGO_DB}?retryWrites=true&w=majority`;

app.use(cors());
app.use(bodyParser.json());

app.post("/hook", (req, res) => {
  console.log("Response from paytm", JSON.stringify(req.body));
  res.status(200).end();
});

app.get("/test-mail", (req, res) => {
  console.log(JSON.stringify(req.body));
  let mailService = new MailUtils();
  mailService.sendMail(
    "anksshady@gmail.com",
    "Hello there!",
    "Hello from grapes.com"
  );
  res.status(200).end();
});

//any app.use() goes before this line
server.applyMiddleware({ app });

mongoose
  .connect(MONGO_URL, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => {
    app.listen({ port: process.env.PORT || 4000 });
    console.log("Server is Running!");
  })
  .catch((err) => {
    console.log(err);
  });

export default app;
