import { gql } from "apollo-server-express";

export default gql`
  #domainInfoMap for both student and examExpert maybe different
  type ExamExpert {
    id: ID!
    name: String!
    email: String!
    profilePic: String
    phone: String
    domainInfoMap: String!
    schedulingUrl: String!
      linkedinUrl: String!
    sessionPrice: Int!
  }
  type Student {
    id: ID!
    name: String!
    email: String!
    profilePic: String
    phone: String
    domainInfoMap: String!
  }
  type Filter {
    filterType: FilterType!
    field: String!
    operation: FilterOperation!
    value: String!
  }
  type Session {
    id: ID!
    examExpertId: String!
    studentId: String!
    agenda: String!
    videoURL: String
    meetingURL: String!
    startTimestamp: Int!
    studentFeedback: String #student's feedback from examExpert
    examExpertReview: String #exam expert's review from student
    domain: String!
    sessionPrice: Int!
  }
  type Order {
    id: ID!
    examExpertId: String!
    studentId: String!
    agenda: String!
    meetingURL: String!
    startTimestamp: Int!
    domain: String!
    sessionPrice: Int!
  }
`;
