import { gql } from "apollo-server-express";

export default gql`
  input FilterInput {
    filterType: FilterType!
    field: String!
    operation: FilterOperation!
    value: String!
  }

  input AddStudentFeedbackGivenSessionIdInput {
    sessionId: ID!
    studentFeedback: String!
  }
  type AddStudentFeedbackGivenSessionIdResponse {
    session: Session!
  }

  input AddExamExpertReviewGivenSessionIdInput {
    sessionId: ID!
    examExpertReview: String!
  }
  type AddExamExpertReviewGivenSessionIdResponse {
    session: Session!
  }
  input CreateSessionInput {
    orderId: ID!
  }
  input CreateOrderInput {
    examExpertId: ID!
    studentId: ID!
    startTime: Int!
    sessionPrice: Int!
    domain: String!
    agenda: String
  }
  type CreateSessionResponse {
    session: Session!
    meetingLink: String!
  }
  type CreateOrderResponse {
    orderId: ID!
    paymentParams: PaymentParams!
  }
  type PaymentParams {
    MID: String!
    WEBSITE: String!
    INDUSTRY_TYPE_ID: String!
    CHANNEL_ID: String!
    ORDER_ID: String!
    CUST_ID: String!
    MOBILE_NO: String!
    EMAIL: String!
    TXN_AMOUNT: String!
    CALLBACK_URL: String!
    CHECKSUMHASH: String!
    TXN_TOKEN: String!
  }
  #domainInfoMap consists of domain -> info mapping which populates field from config and passed as a string.
  input ApplyExamExpertInput {
    email: String!
    name: String!
    phone: String!
    domainInfoMap: String!
  }
  type ApplyExamExpertResponse {
    applicantId: ID!
  }

  input RegisterExamExpertInput {
    applicantId: ID!
    schedulingUrl: String!
    linkedinUrl: String!
    sessionPrice: Int!
    password: String!
  }
  type RegisterExamExpertResponse {
    examExpert: ExamExpert!
  }

  type SessionValidityResponse {
    isValid: Boolean!
    message: String!
  }
  input LoginExamExpertInput {
    email: String!
    password: String!
  }
  type LoginExamExpertResponse {
    examExpert: ExamExpert!
    token: String!
    tokenExpiration: Int!
  }

  input EditExamExpertInfoInput {
    id: ID!
    name: String!
    profilePic: String
    phone: String!
    domainInfoMap: String!
    sessionPrice: Int!
  }
  type EditExamExpertInfoResponse {
    examExpert: ExamExpert!
  }

  input RegisterStudentInput {
    email: String!
    name: String!
    phone: String!
    profilePic: String
    domainInfoMap: String!
    password: String!
  }
  type RegisterStudentResponse {
    student: Student!
  }

  input LoginStudentInput {
    email: String!
    password: String!
  }
  type LoginStudentResponse {
    student: Student!
    token: String!
    tokenExpiration: Int!
  }

  input EditStudentInfoInput {
    id: ID!
    name: String!
    profilePic: String
    phone: String!
    domainInfoMap: String!
  }

  type EditStudentInfoResponse {
    student: Student!
  }

  input ChangePasswordInput {
    email: String!
    newPassword: String!
    userType: User!
  }
  type ChangePasswordResponse {
    id: String!
    userType: User!
  }
  input ForgotPasswordInput {
    email: String!
    userType: User!
  }
  type ForgotPasswordResponse {
    id: String!
    userType: User!
  }
`;
