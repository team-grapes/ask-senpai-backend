import { gql } from "apollo-server-express";

export default gql`
  type Mutation {
    applyForExamExpert(input: ApplyExamExpertInput!): ApplyExamExpertResponse!

    #Triggered from backend, not exposed to frontend
    registerExamExpert(
      input: RegisterExamExpertInput!
    ): RegisterExamExpertResponse!

    loginExamExpert(input: LoginExamExpertInput!): LoginExamExpertResponse!

    registerStudent(input: RegisterStudentInput!): RegisterStudentResponse!
    loginStudent(input: LoginStudentInput!): LoginStudentResponse!

    editExamExpertInfo(
      input: EditExamExpertInfoInput!
    ): EditExamExpertInfoResponse!

    editStudentInfo(input: EditStudentInfoInput!): EditStudentInfoResponse!

    addStudentFeedbackGivenSessionId(
      input: AddStudentFeedbackGivenSessionIdInput!
    ): AddStudentFeedbackGivenSessionIdResponse!

    addExamExpertReviewGivenSessionId(
      input: AddExamExpertReviewGivenSessionIdInput!
    ): AddExamExpertReviewGivenSessionIdResponse!

    createSession(input: CreateSessionInput!): CreateSessionResponse!

    createOrder(input: CreateOrderInput!): CreateOrderResponse!
    changePassword(input: ChangePasswordInput!): ChangePasswordResponse!
    forgotPassword(input: ForgotPasswordInput!): ForgotPasswordResponse!
  }
`;
