import { gql } from "apollo-server-express";

export default gql`
  type Query {
    helloWorld: String!
    getStudentGivenId(studentId: ID!): Student!
    getExamExpertGivenId(examExpertId: ID!): ExamExpert!

    getSessionsGivenStudentId(studentId: ID!): [Session!]!
    getSessionsGivenExamExpertId(examExpertId: ID!): [Session!]!

    getSessionInfoGivenId(sessionId: ID!): Session!
    getExamExpertsGivenFilters(input: [FilterInput!]!): [ExamExpert!]!
    checkIfSessionIsValid(input: CreateOrderInput!): SessionValidityResponse!
  }
`;
