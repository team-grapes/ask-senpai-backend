import { gql } from "apollo-server-express";

export default gql`
  enum Domain {
    IIT
    CAT
  }
  enum FilterOperation {
    LTE
    EQL
    GTE
  }
  enum FilterType {
    String
    Number
  }
  
  enum User {
      EXAMEXPERT
      STUDENT
  }
`;
