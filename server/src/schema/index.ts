import commonEnums from "./typeDefs/commonEnums";
import commonTypes from "./typeDefs/commonTypes";
import queryTypeDefs from "./typeDefs/query";
import mutationTypeDefs from "./typeDefs/mutation";
import schema from "./typeDefs/schema";
import coreEntities from "./typeDefs/coreEntities";

export default [
  commonEnums,
  commonTypes,
  coreEntities,
  queryTypeDefs,
  mutationTypeDefs,
  schema
];
