import mongoose, {Document, Model} from "mongoose";
const Schema = mongoose.Schema;
const examExpertApplicantSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        required: true
    },
    domainInfoMap :{
        type: Schema.Types.Mixed,
        required : true
    },
    timestamp: { type: Number, default: Date.now},
});
export interface IExamExpertApplicant extends Document{
    name: string;
    email: string;
    phone: string;
    timestamp: number;
    domainInfoMap: unknown;
}
export const ExamExpertApplicantModel: Model<IExamExpertApplicant> = mongoose.model('ExamExpertApplicant', examExpertApplicantSchema);
