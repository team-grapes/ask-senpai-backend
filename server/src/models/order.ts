import mongoose, { Document, Model } from "mongoose";
const Schema = mongoose.Schema;

const orderSchema = new Schema({
  examExpertId: {
    type: String,
    required: true,
  },
  studentId: {
    type: String,
    required: true,
  },
  agenda: {
    type: String,
    required: true,
  },
  domain: {
    type: String,
    required: true,
  },
  startTimestamp: {
    type: Number,
    required: true,
  },
  sessionPrice: {
    type: Number,
    required: true,
  },
  timestamp: { type: Number, default: Date.now}
});
export interface IOrder extends Document {
  examExpertId: string;
  studentId: string;
  agenda: string;
  domain: string;
  startTimestamp: number;
  sessionPrice: number;
  timestamp: number;
}

export const OrderModel: Model<IOrder> = mongoose.model("Order", orderSchema);
