import mongoose, { Document, Model } from "mongoose";
const Schema = mongoose.Schema;
const examExpertSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  phone: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  profilePic: {
    type: String,
    required: false,
  },
  domainInfoMap: {
    type: Schema.Types.Mixed,
    required: true,
  },
  schedulingUrl: {
    type: String,
    required: true,
  },
  sessionPrice: {
    type: Number,
    required: true,
  },
  linkedinUrl: {
    type: String,
    required: true,
  },
  timestamp: { type: Number, default: Date.now},
});
export interface IExamExpert extends Document {
  name: string;
  email: string;
  password: string;
  phone: string;
  profilePic?: string | null;
  domainInfoMap: unknown;
  schedulingUrl: string;
  sessionPrice: number;
  linkedinUrl: string;
  timestamp: number;
}
export const ExamExpertModel: Model<IExamExpert> = mongoose.model(
  "ExamExpert",
  examExpertSchema
);
