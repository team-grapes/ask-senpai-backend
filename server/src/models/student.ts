import mongoose, {Document, Model} from "mongoose";
const Schema = mongoose.Schema;
const studentSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        required: true
    },
    profilePic: {
        type: String,
        required: false
    },
    domainInfoMap: {
        type: Schema.Types.Mixed,
        required : true
    },
    timestamp: { type: Number, default: Date.now}
});
export interface IStudent extends Document {
    name: string;
    email: string;
    password: string;
    phone: string
    profilePic?: string | null;
    domainInfoMap: unknown;
    timestamp: number;
}
export const StudentModel: Model<IStudent> = mongoose.model('Student', studentSchema);
