import mongoose, { Document, Model } from "mongoose";
const Schema = mongoose.Schema;

const sessionSchema = new Schema({
  examExpertId: {
    type: String,
    required: true,
  },
  studentId: {
    type: String,
    required: true,
  },
  agenda: {
    type: String,
    required: true,
  },
  videoURL: {
    type: String,
    required: false,
  },
  domain: {
    type: String,
    required: true,
  },
  meetingURL: {
    type: String,
    required: true,
  },
  startTimestamp: {
    type: Number,
    required: true,
  },
  studentFeedback: {
    type: String,
    required: false,
  },
  examExpertReview: {
    type: String,
    required: false,
  },
  sessionPrice: {
    type: Number,
    required: true,
  },
  timestamp: { type: Number, default: Date.now}
});
export interface ISession extends Document {
  examExpertId: string;
  studentId: string;
  agenda: string;
  videoURL?: string | null;
  domain: string;
  meetingURL: string;
  startTimestamp: number;
  studentFeedback?: string | null;
  examExpertReview?: string | null;
  sessionPrice: number;
  timestamp: number;
}

export const SessionModel: Model<ISession> = mongoose.model(
  "Session",
  sessionSchema
);
