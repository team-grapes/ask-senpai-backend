import { MailUtils } from "../utils/mailUtils";

const sendBookingEmail = (
  examExpertName: string,
  examExpertEmail: string,
  studentName: string,
  studentEmail: string,
  meetingURL: string,
  startTimestamp: number,
  agenda: string,
  domain: string
) => {
  let startTime = new Date(startTimestamp * 1000);
  let dateString = startTime.toString();
  console.log(dateString);
  let mailService = new MailUtils();

  mailService.sendMail(
    studentEmail,
    "Your session with " + examExpertName + " is confirmed on Grapes",
    "Dear " +
      studentName +
      ",\n" +
      "We are looking forward to having a fruitful mentorship session with " +
      examExpertName +
      " at this link " +
      meetingURL +
      " on: " +
      dateString +
      " \n" +
      "You’ve booked a 30 minute session on " +
      domain +
      " with the agenda " +
      agenda +
      ".\n" +
      "If, for any technical reason, you can’t join the session, please let us know as soon as possible by giving us a call on 9534291132.\n" +
      "\n" +
      "Kind Regards,\n" +
      "The Grapes Team\n"
  );

  mailService.sendMail(
    examExpertEmail,
    "New session booked with " + studentName + " on Grapes",
    "Dear " +
      examExpertName +
      ",\n" +
      "We are looking forward to having a fruitful mentorship session with " +
      studentName +
      " at this link " +
      meetingURL +
      " on:\n" +
      dateString +
      " \n" +
      "The student has booked a 30 minute session on " +
      domain +
      " with the agenda " +
      agenda +
      ".\n" +
      "If, for any reason, you can’t join the session, please let us know as soon as possible by giving us a call on 9534291132.\n" +
      "\n" +
      "Kind Regards,\n" +
      "The Grapes Team\n"
  );
};

export default sendBookingEmail;
