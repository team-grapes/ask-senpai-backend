const jwt = require("jsonwebtoken");
require("dotenv").config();

const jwtSecretKey = process.env.JWT_SECRET_KEY;

const context = async ({ req }: any) => {
  //console.log("Got this request now", req);
  const authHeader = req.get("Authorization");
  //console.log(authHeader);
  if (!authHeader) {
    req.isAuth = false;
    return { req };
  }
  const token = authHeader.split(" ")[1];
  //console.log(token);
  if (!token || token === "") {
    req.isAuth = false;
    return { req };
  }
  let decodedToken;
  try {
    decodedToken = jwt.verify(token, jwtSecretKey);
  } catch (err) {
    req.isAuth = false;
    return { req };
  }
  if (!decodedToken) {
    req.isAuth = false;
    return { req };
  }
  req.isAuth = true;
  req.userId = decodedToken.userId;
  //console.log(req.isAuth, req.userId);
  return { req };
};

export default context;
