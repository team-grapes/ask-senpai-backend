import { GraphQLResolveInfo } from "graphql";
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = {
  [K in keyof T]: T[K];
};
export type MakeOptional<T, K extends keyof T> = Omit<T, K> &
  { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> &
  { [SubKey in K]: Maybe<T[SubKey]> };
export type RequireFields<T, K extends keyof T> = {
  [X in Exclude<keyof T, K>]?: T[X];
} &
  { [P in K]-?: NonNullable<T[P]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

export enum Domain {
  Iit = "IIT",
  Cat = "CAT",
}

export enum FilterOperation {
  Lte = "LTE",
  Eql = "EQL",
  Gte = "GTE",
}

export enum FilterType {
  String = "String",
  Number = "Number",
}

export enum User {
  Examexpert = "EXAMEXPERT",
  Student = "STUDENT",
}

export type FilterInput = {
  filterType: FilterType;
  field: Scalars["String"];
  operation: FilterOperation;
  value: Scalars["String"];
};

export type AddStudentFeedbackGivenSessionIdInput = {
  sessionId: Scalars["ID"];
  studentFeedback: Scalars["String"];
};

export type AddStudentFeedbackGivenSessionIdResponse = {
  __typename?: "AddStudentFeedbackGivenSessionIdResponse";
  session: Session;
};

export type AddExamExpertReviewGivenSessionIdInput = {
  sessionId: Scalars["ID"];
  examExpertReview: Scalars["String"];
};

export type AddExamExpertReviewGivenSessionIdResponse = {
  __typename?: "AddExamExpertReviewGivenSessionIdResponse";
  session: Session;
};

export type CreateSessionInput = {
  orderId: Scalars["ID"];
};

export type CreateOrderInput = {
  examExpertId: Scalars["ID"];
  studentId: Scalars["ID"];
  startTime: Scalars["Int"];
  sessionPrice: Scalars["Int"];
  domain: Scalars["String"];
  agenda?: Maybe<Scalars["String"]>;
};

export type CreateSessionResponse = {
  __typename?: "CreateSessionResponse";
  session: Session;
  meetingLink: Scalars["String"];
};

export type CreateOrderResponse = {
  __typename?: "CreateOrderResponse";
  orderId: Scalars["ID"];
  paymentParams: PaymentParams;
};

export type PaymentParams = {
  __typename?: "PaymentParams";
  MID: Scalars["String"];
  WEBSITE: Scalars["String"];
  INDUSTRY_TYPE_ID: Scalars["String"];
  CHANNEL_ID: Scalars["String"];
  ORDER_ID: Scalars["String"];
  CUST_ID: Scalars["String"];
  MOBILE_NO: Scalars["String"];
  EMAIL: Scalars["String"];
  TXN_AMOUNT: Scalars["String"];
  CALLBACK_URL: Scalars["String"];
  CHECKSUMHASH: Scalars["String"];
  TXN_TOKEN: Scalars["String"];
};

export type ApplyExamExpertInput = {
  email: Scalars["String"];
  name: Scalars["String"];
  phone: Scalars["String"];
  domainInfoMap: Scalars["String"];
};

export type ApplyExamExpertResponse = {
  __typename?: "ApplyExamExpertResponse";
  applicantId: Scalars["ID"];
};

export type RegisterExamExpertInput = {
  applicantId: Scalars["ID"];
  schedulingUrl: Scalars["String"];
  linkedinUrl: Scalars["String"];
  sessionPrice: Scalars["Int"];
  password: Scalars["String"];
};

export type RegisterExamExpertResponse = {
  __typename?: "RegisterExamExpertResponse";
  examExpert: ExamExpert;
};

export type SessionValidityResponse = {
  __typename?: "SessionValidityResponse";
  isValid: Scalars["Boolean"];
  message: Scalars["String"];
};

export type LoginExamExpertInput = {
  email: Scalars["String"];
  password: Scalars["String"];
};

export type LoginExamExpertResponse = {
  __typename?: "LoginExamExpertResponse";
  examExpert: ExamExpert;
  token: Scalars["String"];
  tokenExpiration: Scalars["Int"];
};

export type EditExamExpertInfoInput = {
  id: Scalars["ID"];
  name: Scalars["String"];
  profilePic?: Maybe<Scalars["String"]>;
  phone: Scalars["String"];
  domainInfoMap: Scalars["String"];
  sessionPrice: Scalars["Int"];
};

export type EditExamExpertInfoResponse = {
  __typename?: "EditExamExpertInfoResponse";
  examExpert: ExamExpert;
};

export type RegisterStudentInput = {
  email: Scalars["String"];
  name: Scalars["String"];
  phone: Scalars["String"];
  profilePic?: Maybe<Scalars["String"]>;
  domainInfoMap: Scalars["String"];
  password: Scalars["String"];
};

export type RegisterStudentResponse = {
  __typename?: "RegisterStudentResponse";
  student: Student;
};

export type LoginStudentInput = {
  email: Scalars["String"];
  password: Scalars["String"];
};

export type LoginStudentResponse = {
  __typename?: "LoginStudentResponse";
  student: Student;
  token: Scalars["String"];
  tokenExpiration: Scalars["Int"];
};

export type EditStudentInfoInput = {
  id: Scalars["ID"];
  name: Scalars["String"];
  profilePic?: Maybe<Scalars["String"]>;
  phone: Scalars["String"];
  domainInfoMap: Scalars["String"];
};

export type EditStudentInfoResponse = {
  __typename?: "EditStudentInfoResponse";
  student: Student;
};

export type ChangePasswordInput = {
  email: Scalars["String"];
  newPassword: Scalars["String"];
  userType: User;
};

export type ChangePasswordResponse = {
  __typename?: "ChangePasswordResponse";
  id: Scalars["String"];
  userType: User;
};

export type ForgotPasswordInput = {
  email: Scalars["String"];
  userType: User;
};

export type ForgotPasswordResponse = {
  __typename?: "ForgotPasswordResponse";
  id: Scalars["String"];
  userType: User;
};

export type ExamExpert = {
  __typename?: "ExamExpert";
  id: Scalars["ID"];
  name: Scalars["String"];
  email: Scalars["String"];
  profilePic?: Maybe<Scalars["String"]>;
  phone?: Maybe<Scalars["String"]>;
  domainInfoMap: Scalars["String"];
  schedulingUrl: Scalars["String"];
  linkedinUrl: Scalars["String"];
  sessionPrice: Scalars["Int"];
};

export type Student = {
  __typename?: "Student";
  id: Scalars["ID"];
  name: Scalars["String"];
  email: Scalars["String"];
  profilePic?: Maybe<Scalars["String"]>;
  phone?: Maybe<Scalars["String"]>;
  domainInfoMap: Scalars["String"];
};

export type Filter = {
  __typename?: "Filter";
  filterType: FilterType;
  field: Scalars["String"];
  operation: FilterOperation;
  value: Scalars["String"];
};

export type Session = {
  __typename?: "Session";
  id: Scalars["ID"];
  examExpertId: Scalars["String"];
  studentId: Scalars["String"];
  agenda: Scalars["String"];
  videoURL?: Maybe<Scalars["String"]>;
  meetingURL: Scalars["String"];
  startTimestamp: Scalars["Int"];
  studentFeedback?: Maybe<Scalars["String"]>;
  examExpertReview?: Maybe<Scalars["String"]>;
  domain: Scalars["String"];
  sessionPrice: Scalars["Int"];
};

export type Order = {
  __typename?: "Order";
  id: Scalars["ID"];
  examExpertId: Scalars["String"];
  studentId: Scalars["String"];
  agenda: Scalars["String"];
  meetingURL: Scalars["String"];
  startTimestamp: Scalars["Int"];
  domain: Scalars["String"];
  sessionPrice: Scalars["Int"];
};

export type Mutation = {
  __typename?: "Mutation";
  applyForExamExpert: ApplyExamExpertResponse;
  registerExamExpert: RegisterExamExpertResponse;
  loginExamExpert: LoginExamExpertResponse;
  registerStudent: RegisterStudentResponse;
  loginStudent: LoginStudentResponse;
  editExamExpertInfo: EditExamExpertInfoResponse;
  editStudentInfo: EditStudentInfoResponse;
  addStudentFeedbackGivenSessionId: AddStudentFeedbackGivenSessionIdResponse;
  addExamExpertReviewGivenSessionId: AddExamExpertReviewGivenSessionIdResponse;
  createSession: CreateSessionResponse;
  createOrder: CreateOrderResponse;
  changePassword: ChangePasswordResponse;
  forgotPassword: ForgotPasswordResponse;
};

export type MutationApplyForExamExpertArgs = {
  input: ApplyExamExpertInput;
};

export type MutationRegisterExamExpertArgs = {
  input: RegisterExamExpertInput;
};

export type MutationLoginExamExpertArgs = {
  input: LoginExamExpertInput;
};

export type MutationRegisterStudentArgs = {
  input: RegisterStudentInput;
};

export type MutationLoginStudentArgs = {
  input: LoginStudentInput;
};

export type MutationEditExamExpertInfoArgs = {
  input: EditExamExpertInfoInput;
};

export type MutationEditStudentInfoArgs = {
  input: EditStudentInfoInput;
};

export type MutationAddStudentFeedbackGivenSessionIdArgs = {
  input: AddStudentFeedbackGivenSessionIdInput;
};

export type MutationAddExamExpertReviewGivenSessionIdArgs = {
  input: AddExamExpertReviewGivenSessionIdInput;
};

export type MutationCreateSessionArgs = {
  input: CreateSessionInput;
};

export type MutationCreateOrderArgs = {
  input: CreateOrderInput;
};

export type MutationChangePasswordArgs = {
  input: ChangePasswordInput;
};

export type MutationForgotPasswordArgs = {
  input: ForgotPasswordInput;
};

export type Query = {
  __typename?: "Query";
  helloWorld: Scalars["String"];
  getStudentGivenId: Student;
  getExamExpertGivenId: ExamExpert;
  getSessionsGivenStudentId: Array<Session>;
  getSessionsGivenExamExpertId: Array<Session>;
  getSessionInfoGivenId: Session;
  getExamExpertsGivenFilters: Array<ExamExpert>;
  checkIfSessionIsValid: SessionValidityResponse;
};

export type QueryGetStudentGivenIdArgs = {
  studentId: Scalars["ID"];
};

export type QueryGetExamExpertGivenIdArgs = {
  examExpertId: Scalars["ID"];
};

export type QueryGetSessionsGivenStudentIdArgs = {
  studentId: Scalars["ID"];
};

export type QueryGetSessionsGivenExamExpertIdArgs = {
  examExpertId: Scalars["ID"];
};

export type QueryGetSessionInfoGivenIdArgs = {
  sessionId: Scalars["ID"];
};

export type QueryGetExamExpertsGivenFiltersArgs = {
  input: Array<FilterInput>;
};

export type QueryCheckIfSessionIsValidArgs = {
  input: CreateOrderInput;
};

export type AdditionalEntityFields = {
  path?: Maybe<Scalars["String"]>;
  type?: Maybe<Scalars["String"]>;
};

export type ResolverTypeWrapper<T> = Promise<T> | T;

export type LegacyStitchingResolver<TResult, TParent, TContext, TArgs> = {
  fragment: string;
  resolve: ResolverFn<TResult, TParent, TContext, TArgs>;
};

export type NewStitchingResolver<TResult, TParent, TContext, TArgs> = {
  selectionSet: string;
  resolve: ResolverFn<TResult, TParent, TContext, TArgs>;
};
export type StitchingResolver<TResult, TParent, TContext, TArgs> =
  | LegacyStitchingResolver<TResult, TParent, TContext, TArgs>
  | NewStitchingResolver<TResult, TParent, TContext, TArgs>;
export type Resolver<TResult, TParent = {}, TContext = {}, TArgs = {}> =
  | ResolverFn<TResult, TParent, TContext, TArgs>
  | StitchingResolver<TResult, TParent, TContext, TArgs>;

export type ResolverFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => Promise<TResult> | TResult;

export type SubscriptionSubscribeFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => AsyncIterator<TResult> | Promise<AsyncIterator<TResult>>;

export type SubscriptionResolveFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

export interface SubscriptionSubscriberObject<
  TResult,
  TKey extends string,
  TParent,
  TContext,
  TArgs
> {
  subscribe: SubscriptionSubscribeFn<
    { [key in TKey]: TResult },
    TParent,
    TContext,
    TArgs
  >;
  resolve?: SubscriptionResolveFn<
    TResult,
    { [key in TKey]: TResult },
    TContext,
    TArgs
  >;
}

export interface SubscriptionResolverObject<TResult, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<any, TParent, TContext, TArgs>;
  resolve: SubscriptionResolveFn<TResult, any, TContext, TArgs>;
}

export type SubscriptionObject<
  TResult,
  TKey extends string,
  TParent,
  TContext,
  TArgs
> =
  | SubscriptionSubscriberObject<TResult, TKey, TParent, TContext, TArgs>
  | SubscriptionResolverObject<TResult, TParent, TContext, TArgs>;

export type SubscriptionResolver<
  TResult,
  TKey extends string,
  TParent = {},
  TContext = {},
  TArgs = {}
> =
  | ((
      ...args: any[]
    ) => SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>)
  | SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>;

export type TypeResolveFn<TTypes, TParent = {}, TContext = {}> = (
  parent: TParent,
  context: TContext,
  info: GraphQLResolveInfo
) => Maybe<TTypes> | Promise<Maybe<TTypes>>;

export type IsTypeOfResolverFn<T = {}, TContext = {}> = (
  obj: T,
  context: TContext,
  info: GraphQLResolveInfo
) => boolean | Promise<boolean>;

export type NextResolverFn<T> = () => Promise<T>;

export type DirectiveResolverFn<
  TResult = {},
  TParent = {},
  TContext = {},
  TArgs = {}
> = (
  next: NextResolverFn<TResult>,
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

/** Mapping between all available schema types and the resolvers types */
export type ResolversTypes = {
  Domain: Domain;
  FilterOperation: FilterOperation;
  FilterType: FilterType;
  User: User;
  FilterInput: FilterInput;
  String: ResolverTypeWrapper<Scalars["String"]>;
  AddStudentFeedbackGivenSessionIdInput: AddStudentFeedbackGivenSessionIdInput;
  ID: ResolverTypeWrapper<Scalars["ID"]>;
  AddStudentFeedbackGivenSessionIdResponse: ResolverTypeWrapper<AddStudentFeedbackGivenSessionIdResponse>;
  AddExamExpertReviewGivenSessionIdInput: AddExamExpertReviewGivenSessionIdInput;
  AddExamExpertReviewGivenSessionIdResponse: ResolverTypeWrapper<AddExamExpertReviewGivenSessionIdResponse>;
  CreateSessionInput: CreateSessionInput;
  CreateOrderInput: CreateOrderInput;
  Int: ResolverTypeWrapper<Scalars["Int"]>;
  CreateSessionResponse: ResolverTypeWrapper<CreateSessionResponse>;
  CreateOrderResponse: ResolverTypeWrapper<CreateOrderResponse>;
  PaymentParams: ResolverTypeWrapper<PaymentParams>;
  ApplyExamExpertInput: ApplyExamExpertInput;
  ApplyExamExpertResponse: ResolverTypeWrapper<ApplyExamExpertResponse>;
  RegisterExamExpertInput: RegisterExamExpertInput;
  RegisterExamExpertResponse: ResolverTypeWrapper<RegisterExamExpertResponse>;
  SessionValidityResponse: ResolverTypeWrapper<SessionValidityResponse>;
  Boolean: ResolverTypeWrapper<Scalars["Boolean"]>;
  LoginExamExpertInput: LoginExamExpertInput;
  LoginExamExpertResponse: ResolverTypeWrapper<LoginExamExpertResponse>;
  EditExamExpertInfoInput: EditExamExpertInfoInput;
  EditExamExpertInfoResponse: ResolverTypeWrapper<EditExamExpertInfoResponse>;
  RegisterStudentInput: RegisterStudentInput;
  RegisterStudentResponse: ResolverTypeWrapper<RegisterStudentResponse>;
  LoginStudentInput: LoginStudentInput;
  LoginStudentResponse: ResolverTypeWrapper<LoginStudentResponse>;
  EditStudentInfoInput: EditStudentInfoInput;
  EditStudentInfoResponse: ResolverTypeWrapper<EditStudentInfoResponse>;
  ChangePasswordInput: ChangePasswordInput;
  ChangePasswordResponse: ResolverTypeWrapper<ChangePasswordResponse>;
  ForgotPasswordInput: ForgotPasswordInput;
  ForgotPasswordResponse: ResolverTypeWrapper<ForgotPasswordResponse>;
  ExamExpert: ResolverTypeWrapper<ExamExpert>;
  Student: ResolverTypeWrapper<Student>;
  Filter: ResolverTypeWrapper<Filter>;
  Session: ResolverTypeWrapper<Session>;
  Order: ResolverTypeWrapper<Order>;
  Mutation: ResolverTypeWrapper<{}>;
  Query: ResolverTypeWrapper<{}>;
  AdditionalEntityFields: AdditionalEntityFields;
};

/** Mapping between all available schema types and the resolvers parents */
export type ResolversParentTypes = {
  FilterInput: FilterInput;
  String: Scalars["String"];
  AddStudentFeedbackGivenSessionIdInput: AddStudentFeedbackGivenSessionIdInput;
  ID: Scalars["ID"];
  AddStudentFeedbackGivenSessionIdResponse: AddStudentFeedbackGivenSessionIdResponse;
  AddExamExpertReviewGivenSessionIdInput: AddExamExpertReviewGivenSessionIdInput;
  AddExamExpertReviewGivenSessionIdResponse: AddExamExpertReviewGivenSessionIdResponse;
  CreateSessionInput: CreateSessionInput;
  CreateOrderInput: CreateOrderInput;
  Int: Scalars["Int"];
  CreateSessionResponse: CreateSessionResponse;
  CreateOrderResponse: CreateOrderResponse;
  PaymentParams: PaymentParams;
  ApplyExamExpertInput: ApplyExamExpertInput;
  ApplyExamExpertResponse: ApplyExamExpertResponse;
  RegisterExamExpertInput: RegisterExamExpertInput;
  RegisterExamExpertResponse: RegisterExamExpertResponse;
  SessionValidityResponse: SessionValidityResponse;
  Boolean: Scalars["Boolean"];
  LoginExamExpertInput: LoginExamExpertInput;
  LoginExamExpertResponse: LoginExamExpertResponse;
  EditExamExpertInfoInput: EditExamExpertInfoInput;
  EditExamExpertInfoResponse: EditExamExpertInfoResponse;
  RegisterStudentInput: RegisterStudentInput;
  RegisterStudentResponse: RegisterStudentResponse;
  LoginStudentInput: LoginStudentInput;
  LoginStudentResponse: LoginStudentResponse;
  EditStudentInfoInput: EditStudentInfoInput;
  EditStudentInfoResponse: EditStudentInfoResponse;
  ChangePasswordInput: ChangePasswordInput;
  ChangePasswordResponse: ChangePasswordResponse;
  ForgotPasswordInput: ForgotPasswordInput;
  ForgotPasswordResponse: ForgotPasswordResponse;
  ExamExpert: ExamExpert;
  Student: Student;
  Filter: Filter;
  Session: Session;
  Order: Order;
  Mutation: {};
  Query: {};
  AdditionalEntityFields: AdditionalEntityFields;
};

export type UnionDirectiveArgs = {
  discriminatorField?: Maybe<Scalars["String"]>;
  additionalFields?: Maybe<Array<Maybe<AdditionalEntityFields>>>;
};

export type UnionDirectiveResolver<
  Result,
  Parent,
  ContextType = any,
  Args = UnionDirectiveArgs
> = DirectiveResolverFn<Result, Parent, ContextType, Args>;

export type AbstractEntityDirectiveArgs = {
  discriminatorField: Scalars["String"];
  additionalFields?: Maybe<Array<Maybe<AdditionalEntityFields>>>;
};

export type AbstractEntityDirectiveResolver<
  Result,
  Parent,
  ContextType = any,
  Args = AbstractEntityDirectiveArgs
> = DirectiveResolverFn<Result, Parent, ContextType, Args>;

export type EntityDirectiveArgs = {
  embedded?: Maybe<Scalars["Boolean"]>;
  additionalFields?: Maybe<Array<Maybe<AdditionalEntityFields>>>;
};

export type EntityDirectiveResolver<
  Result,
  Parent,
  ContextType = any,
  Args = EntityDirectiveArgs
> = DirectiveResolverFn<Result, Parent, ContextType, Args>;

export type ColumnDirectiveArgs = { overrideType?: Maybe<Scalars["String"]> };

export type ColumnDirectiveResolver<
  Result,
  Parent,
  ContextType = any,
  Args = ColumnDirectiveArgs
> = DirectiveResolverFn<Result, Parent, ContextType, Args>;

export type IdDirectiveArgs = {};

export type IdDirectiveResolver<
  Result,
  Parent,
  ContextType = any,
  Args = IdDirectiveArgs
> = DirectiveResolverFn<Result, Parent, ContextType, Args>;

export type LinkDirectiveArgs = { overrideType?: Maybe<Scalars["String"]> };

export type LinkDirectiveResolver<
  Result,
  Parent,
  ContextType = any,
  Args = LinkDirectiveArgs
> = DirectiveResolverFn<Result, Parent, ContextType, Args>;

export type EmbeddedDirectiveArgs = {};

export type EmbeddedDirectiveResolver<
  Result,
  Parent,
  ContextType = any,
  Args = EmbeddedDirectiveArgs
> = DirectiveResolverFn<Result, Parent, ContextType, Args>;

export type MapDirectiveArgs = { path: Scalars["String"] };

export type MapDirectiveResolver<
  Result,
  Parent,
  ContextType = any,
  Args = MapDirectiveArgs
> = DirectiveResolverFn<Result, Parent, ContextType, Args>;

export type AddStudentFeedbackGivenSessionIdResponseResolvers<
  ContextType = any,
  ParentType extends ResolversParentTypes["AddStudentFeedbackGivenSessionIdResponse"] = ResolversParentTypes["AddStudentFeedbackGivenSessionIdResponse"]
> = {
  session?: Resolver<ResolversTypes["Session"], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type AddExamExpertReviewGivenSessionIdResponseResolvers<
  ContextType = any,
  ParentType extends ResolversParentTypes["AddExamExpertReviewGivenSessionIdResponse"] = ResolversParentTypes["AddExamExpertReviewGivenSessionIdResponse"]
> = {
  session?: Resolver<ResolversTypes["Session"], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type CreateSessionResponseResolvers<
  ContextType = any,
  ParentType extends ResolversParentTypes["CreateSessionResponse"] = ResolversParentTypes["CreateSessionResponse"]
> = {
  session?: Resolver<ResolversTypes["Session"], ParentType, ContextType>;
  meetingLink?: Resolver<ResolversTypes["String"], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type CreateOrderResponseResolvers<
  ContextType = any,
  ParentType extends ResolversParentTypes["CreateOrderResponse"] = ResolversParentTypes["CreateOrderResponse"]
> = {
  orderId?: Resolver<ResolversTypes["ID"], ParentType, ContextType>;
  paymentParams?: Resolver<
    ResolversTypes["PaymentParams"],
    ParentType,
    ContextType
  >;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type PaymentParamsResolvers<
  ContextType = any,
  ParentType extends ResolversParentTypes["PaymentParams"] = ResolversParentTypes["PaymentParams"]
> = {
  MID?: Resolver<ResolversTypes["String"], ParentType, ContextType>;
  WEBSITE?: Resolver<ResolversTypes["String"], ParentType, ContextType>;
  INDUSTRY_TYPE_ID?: Resolver<
    ResolversTypes["String"],
    ParentType,
    ContextType
  >;
  CHANNEL_ID?: Resolver<ResolversTypes["String"], ParentType, ContextType>;
  ORDER_ID?: Resolver<ResolversTypes["String"], ParentType, ContextType>;
  CUST_ID?: Resolver<ResolversTypes["String"], ParentType, ContextType>;
  MOBILE_NO?: Resolver<ResolversTypes["String"], ParentType, ContextType>;
  EMAIL?: Resolver<ResolversTypes["String"], ParentType, ContextType>;
  TXN_AMOUNT?: Resolver<ResolversTypes["String"], ParentType, ContextType>;
  CALLBACK_URL?: Resolver<ResolversTypes["String"], ParentType, ContextType>;
  CHECKSUMHASH?: Resolver<ResolversTypes["String"], ParentType, ContextType>;
  TXN_TOKEN?: Resolver<ResolversTypes["String"], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type ApplyExamExpertResponseResolvers<
  ContextType = any,
  ParentType extends ResolversParentTypes["ApplyExamExpertResponse"] = ResolversParentTypes["ApplyExamExpertResponse"]
> = {
  applicantId?: Resolver<ResolversTypes["ID"], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type RegisterExamExpertResponseResolvers<
  ContextType = any,
  ParentType extends ResolversParentTypes["RegisterExamExpertResponse"] = ResolversParentTypes["RegisterExamExpertResponse"]
> = {
  examExpert?: Resolver<ResolversTypes["ExamExpert"], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type SessionValidityResponseResolvers<
  ContextType = any,
  ParentType extends ResolversParentTypes["SessionValidityResponse"] = ResolversParentTypes["SessionValidityResponse"]
> = {
  isValid?: Resolver<ResolversTypes["Boolean"], ParentType, ContextType>;
  message?: Resolver<ResolversTypes["String"], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type LoginExamExpertResponseResolvers<
  ContextType = any,
  ParentType extends ResolversParentTypes["LoginExamExpertResponse"] = ResolversParentTypes["LoginExamExpertResponse"]
> = {
  examExpert?: Resolver<ResolversTypes["ExamExpert"], ParentType, ContextType>;
  token?: Resolver<ResolversTypes["String"], ParentType, ContextType>;
  tokenExpiration?: Resolver<ResolversTypes["Int"], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type EditExamExpertInfoResponseResolvers<
  ContextType = any,
  ParentType extends ResolversParentTypes["EditExamExpertInfoResponse"] = ResolversParentTypes["EditExamExpertInfoResponse"]
> = {
  examExpert?: Resolver<ResolversTypes["ExamExpert"], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type RegisterStudentResponseResolvers<
  ContextType = any,
  ParentType extends ResolversParentTypes["RegisterStudentResponse"] = ResolversParentTypes["RegisterStudentResponse"]
> = {
  student?: Resolver<ResolversTypes["Student"], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type LoginStudentResponseResolvers<
  ContextType = any,
  ParentType extends ResolversParentTypes["LoginStudentResponse"] = ResolversParentTypes["LoginStudentResponse"]
> = {
  student?: Resolver<ResolversTypes["Student"], ParentType, ContextType>;
  token?: Resolver<ResolversTypes["String"], ParentType, ContextType>;
  tokenExpiration?: Resolver<ResolversTypes["Int"], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type EditStudentInfoResponseResolvers<
  ContextType = any,
  ParentType extends ResolversParentTypes["EditStudentInfoResponse"] = ResolversParentTypes["EditStudentInfoResponse"]
> = {
  student?: Resolver<ResolversTypes["Student"], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type ChangePasswordResponseResolvers<
  ContextType = any,
  ParentType extends ResolversParentTypes["ChangePasswordResponse"] = ResolversParentTypes["ChangePasswordResponse"]
> = {
  id?: Resolver<ResolversTypes["String"], ParentType, ContextType>;
  userType?: Resolver<ResolversTypes["User"], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type ForgotPasswordResponseResolvers<
  ContextType = any,
  ParentType extends ResolversParentTypes["ForgotPasswordResponse"] = ResolversParentTypes["ForgotPasswordResponse"]
> = {
  id?: Resolver<ResolversTypes["String"], ParentType, ContextType>;
  userType?: Resolver<ResolversTypes["User"], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type ExamExpertResolvers<
  ContextType = any,
  ParentType extends ResolversParentTypes["ExamExpert"] = ResolversParentTypes["ExamExpert"]
> = {
  id?: Resolver<ResolversTypes["ID"], ParentType, ContextType>;
  name?: Resolver<ResolversTypes["String"], ParentType, ContextType>;
  email?: Resolver<ResolversTypes["String"], ParentType, ContextType>;
  profilePic?: Resolver<
    Maybe<ResolversTypes["String"]>,
    ParentType,
    ContextType
  >;
  phone?: Resolver<Maybe<ResolversTypes["String"]>, ParentType, ContextType>;
  domainInfoMap?: Resolver<ResolversTypes["String"], ParentType, ContextType>;
  schedulingUrl?: Resolver<ResolversTypes["String"], ParentType, ContextType>;
  linkedinUrl?: Resolver<ResolversTypes["String"], ParentType, ContextType>;
  sessionPrice?: Resolver<ResolversTypes["Int"], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type StudentResolvers<
  ContextType = any,
  ParentType extends ResolversParentTypes["Student"] = ResolversParentTypes["Student"]
> = {
  id?: Resolver<ResolversTypes["ID"], ParentType, ContextType>;
  name?: Resolver<ResolversTypes["String"], ParentType, ContextType>;
  email?: Resolver<ResolversTypes["String"], ParentType, ContextType>;
  profilePic?: Resolver<
    Maybe<ResolversTypes["String"]>,
    ParentType,
    ContextType
  >;
  phone?: Resolver<Maybe<ResolversTypes["String"]>, ParentType, ContextType>;
  domainInfoMap?: Resolver<ResolversTypes["String"], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type FilterResolvers<
  ContextType = any,
  ParentType extends ResolversParentTypes["Filter"] = ResolversParentTypes["Filter"]
> = {
  filterType?: Resolver<ResolversTypes["FilterType"], ParentType, ContextType>;
  field?: Resolver<ResolversTypes["String"], ParentType, ContextType>;
  operation?: Resolver<
    ResolversTypes["FilterOperation"],
    ParentType,
    ContextType
  >;
  value?: Resolver<ResolversTypes["String"], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type SessionResolvers<
  ContextType = any,
  ParentType extends ResolversParentTypes["Session"] = ResolversParentTypes["Session"]
> = {
  id?: Resolver<ResolversTypes["ID"], ParentType, ContextType>;
  examExpertId?: Resolver<ResolversTypes["String"], ParentType, ContextType>;
  studentId?: Resolver<ResolversTypes["String"], ParentType, ContextType>;
  agenda?: Resolver<ResolversTypes["String"], ParentType, ContextType>;
  videoURL?: Resolver<Maybe<ResolversTypes["String"]>, ParentType, ContextType>;
  meetingURL?: Resolver<ResolversTypes["String"], ParentType, ContextType>;
  startTimestamp?: Resolver<ResolversTypes["Int"], ParentType, ContextType>;
  studentFeedback?: Resolver<
    Maybe<ResolversTypes["String"]>,
    ParentType,
    ContextType
  >;
  examExpertReview?: Resolver<
    Maybe<ResolversTypes["String"]>,
    ParentType,
    ContextType
  >;
  domain?: Resolver<ResolversTypes["String"], ParentType, ContextType>;
  sessionPrice?: Resolver<ResolversTypes["Int"], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type OrderResolvers<
  ContextType = any,
  ParentType extends ResolversParentTypes["Order"] = ResolversParentTypes["Order"]
> = {
  id?: Resolver<ResolversTypes["ID"], ParentType, ContextType>;
  examExpertId?: Resolver<ResolversTypes["String"], ParentType, ContextType>;
  studentId?: Resolver<ResolversTypes["String"], ParentType, ContextType>;
  agenda?: Resolver<ResolversTypes["String"], ParentType, ContextType>;
  meetingURL?: Resolver<ResolversTypes["String"], ParentType, ContextType>;
  startTimestamp?: Resolver<ResolversTypes["Int"], ParentType, ContextType>;
  domain?: Resolver<ResolversTypes["String"], ParentType, ContextType>;
  sessionPrice?: Resolver<ResolversTypes["Int"], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type MutationResolvers<
  ContextType = any,
  ParentType extends ResolversParentTypes["Mutation"] = ResolversParentTypes["Mutation"]
> = {
  applyForExamExpert?: Resolver<
    ResolversTypes["ApplyExamExpertResponse"],
    ParentType,
    ContextType,
    RequireFields<MutationApplyForExamExpertArgs, "input">
  >;
  registerExamExpert?: Resolver<
    ResolversTypes["RegisterExamExpertResponse"],
    ParentType,
    ContextType,
    RequireFields<MutationRegisterExamExpertArgs, "input">
  >;
  loginExamExpert?: Resolver<
    ResolversTypes["LoginExamExpertResponse"],
    ParentType,
    ContextType,
    RequireFields<MutationLoginExamExpertArgs, "input">
  >;
  registerStudent?: Resolver<
    ResolversTypes["RegisterStudentResponse"],
    ParentType,
    ContextType,
    RequireFields<MutationRegisterStudentArgs, "input">
  >;
  loginStudent?: Resolver<
    ResolversTypes["LoginStudentResponse"],
    ParentType,
    ContextType,
    RequireFields<MutationLoginStudentArgs, "input">
  >;
  editExamExpertInfo?: Resolver<
    ResolversTypes["EditExamExpertInfoResponse"],
    ParentType,
    ContextType,
    RequireFields<MutationEditExamExpertInfoArgs, "input">
  >;
  editStudentInfo?: Resolver<
    ResolversTypes["EditStudentInfoResponse"],
    ParentType,
    ContextType,
    RequireFields<MutationEditStudentInfoArgs, "input">
  >;
  addStudentFeedbackGivenSessionId?: Resolver<
    ResolversTypes["AddStudentFeedbackGivenSessionIdResponse"],
    ParentType,
    ContextType,
    RequireFields<MutationAddStudentFeedbackGivenSessionIdArgs, "input">
  >;
  addExamExpertReviewGivenSessionId?: Resolver<
    ResolversTypes["AddExamExpertReviewGivenSessionIdResponse"],
    ParentType,
    ContextType,
    RequireFields<MutationAddExamExpertReviewGivenSessionIdArgs, "input">
  >;
  createSession?: Resolver<
    ResolversTypes["CreateSessionResponse"],
    ParentType,
    ContextType,
    RequireFields<MutationCreateSessionArgs, "input">
  >;
  createOrder?: Resolver<
    ResolversTypes["CreateOrderResponse"],
    ParentType,
    ContextType,
    RequireFields<MutationCreateOrderArgs, "input">
  >;
  changePassword?: Resolver<
    ResolversTypes["ChangePasswordResponse"],
    ParentType,
    ContextType,
    RequireFields<MutationChangePasswordArgs, "input">
  >;
  forgotPassword?: Resolver<
    ResolversTypes["ForgotPasswordResponse"],
    ParentType,
    ContextType,
    RequireFields<MutationForgotPasswordArgs, "input">
  >;
};

export type QueryResolvers<
  ContextType = any,
  ParentType extends ResolversParentTypes["Query"] = ResolversParentTypes["Query"]
> = {
  helloWorld?: Resolver<ResolversTypes["String"], ParentType, ContextType>;
  getStudentGivenId?: Resolver<
    ResolversTypes["Student"],
    ParentType,
    ContextType,
    RequireFields<QueryGetStudentGivenIdArgs, "studentId">
  >;
  getExamExpertGivenId?: Resolver<
    ResolversTypes["ExamExpert"],
    ParentType,
    ContextType,
    RequireFields<QueryGetExamExpertGivenIdArgs, "examExpertId">
  >;
  getSessionsGivenStudentId?: Resolver<
    Array<ResolversTypes["Session"]>,
    ParentType,
    ContextType,
    RequireFields<QueryGetSessionsGivenStudentIdArgs, "studentId">
  >;
  getSessionsGivenExamExpertId?: Resolver<
    Array<ResolversTypes["Session"]>,
    ParentType,
    ContextType,
    RequireFields<QueryGetSessionsGivenExamExpertIdArgs, "examExpertId">
  >;
  getSessionInfoGivenId?: Resolver<
    ResolversTypes["Session"],
    ParentType,
    ContextType,
    RequireFields<QueryGetSessionInfoGivenIdArgs, "sessionId">
  >;
  getExamExpertsGivenFilters?: Resolver<
    Array<ResolversTypes["ExamExpert"]>,
    ParentType,
    ContextType,
    RequireFields<QueryGetExamExpertsGivenFiltersArgs, "input">
  >;
  checkIfSessionIsValid?: Resolver<
    ResolversTypes["SessionValidityResponse"],
    ParentType,
    ContextType,
    RequireFields<QueryCheckIfSessionIsValidArgs, "input">
  >;
};

export type Resolvers<ContextType = any> = {
  AddStudentFeedbackGivenSessionIdResponse?: AddStudentFeedbackGivenSessionIdResponseResolvers<ContextType>;
  AddExamExpertReviewGivenSessionIdResponse?: AddExamExpertReviewGivenSessionIdResponseResolvers<ContextType>;
  CreateSessionResponse?: CreateSessionResponseResolvers<ContextType>;
  CreateOrderResponse?: CreateOrderResponseResolvers<ContextType>;
  PaymentParams?: PaymentParamsResolvers<ContextType>;
  ApplyExamExpertResponse?: ApplyExamExpertResponseResolvers<ContextType>;
  RegisterExamExpertResponse?: RegisterExamExpertResponseResolvers<ContextType>;
  SessionValidityResponse?: SessionValidityResponseResolvers<ContextType>;
  LoginExamExpertResponse?: LoginExamExpertResponseResolvers<ContextType>;
  EditExamExpertInfoResponse?: EditExamExpertInfoResponseResolvers<ContextType>;
  RegisterStudentResponse?: RegisterStudentResponseResolvers<ContextType>;
  LoginStudentResponse?: LoginStudentResponseResolvers<ContextType>;
  EditStudentInfoResponse?: EditStudentInfoResponseResolvers<ContextType>;
  ChangePasswordResponse?: ChangePasswordResponseResolvers<ContextType>;
  ForgotPasswordResponse?: ForgotPasswordResponseResolvers<ContextType>;
  ExamExpert?: ExamExpertResolvers<ContextType>;
  Student?: StudentResolvers<ContextType>;
  Filter?: FilterResolvers<ContextType>;
  Session?: SessionResolvers<ContextType>;
  Order?: OrderResolvers<ContextType>;
  Mutation?: MutationResolvers<ContextType>;
  Query?: QueryResolvers<ContextType>;
};

/**
 * @deprecated
 * Use "Resolvers" root object instead. If you wish to get "IResolvers", add "typesPrefix: I" to your config.
 */
export type IResolvers<ContextType = any> = Resolvers<ContextType>;
export type DirectiveResolvers<ContextType = any> = {
  union?: UnionDirectiveResolver<any, any, ContextType>;
  abstractEntity?: AbstractEntityDirectiveResolver<any, any, ContextType>;
  entity?: EntityDirectiveResolver<any, any, ContextType>;
  column?: ColumnDirectiveResolver<any, any, ContextType>;
  id?: IdDirectiveResolver<any, any, ContextType>;
  link?: LinkDirectiveResolver<any, any, ContextType>;
  embedded?: EmbeddedDirectiveResolver<any, any, ContextType>;
  map?: MapDirectiveResolver<any, any, ContextType>;
};

/**
 * @deprecated
 * Use "DirectiveResolvers" root object instead. If you wish to get "IDirectiveResolvers", add "typesPrefix: I" to your config.
 */
export type IDirectiveResolvers<
  ContextType = any
> = DirectiveResolvers<ContextType>;
