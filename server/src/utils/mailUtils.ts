import nodemailer from "nodemailer";
require("dotenv").config();

export class MailUtils {
  private _transporter: nodemailer.Transporter;
  constructor() {
    if (process.env.ENV === "production") {
      this._transporter = nodemailer.createTransport(
        `smtps://${process.env.MAIL_USER}%40gmail.com:${process.env.MAIL_PASSWORD}@smtp.gmail.com`
      );
    } else {
      this._transporter = nodemailer.createTransport({
        host: "smtp.mailtrap.io",
        port: 2525,
        auth: {
          user: process.env.MAIL_USER,
          pass: process.env.MAIL_PASSWORD,
        },
      });
    }
  }
  sendMail(to: string, subject: string, content: string) {
    let options = {
      from: "lisa@grapes.com",
      to: to,
      subject: subject,
      text: content,
    };

    this._transporter.sendMail(
      options,
      (error: unknown, info: { response: unknown }) => {
        if (error) {
          return console.log(`error: ${error}`);
        }
        console.log(`Message Sent ${info.response}`);
      }
    );
  }
}
