const Paytm = require("paytm-pg-node-sdk");
require("dotenv").config();

const initiatePayment = async (
  orderID: any,
  studentId: any,
  studentEmail: any,
  studentPhone: any,
  sessionPrice: number
) => {
  if (process.env.ENV === "development") {
    // For Staging
    var environment = Paytm.LibraryConstants.STAGING_ENVIRONMENT;
  } else {
    // For Production
    var environment = Paytm.LibraryConstants.PRODUCTION_ENVIRONMENT;
  }

  // Find your mid, key, website in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys
  var mid = process.env.MERCHANT_ID;
  var key = process.env.MERCHANT_KEY;
  var website = process.env.WEBSITE;
  var callbackUrl = process.env.CALLBACK_URL;
  Paytm.MerchantProperties.setCallbackUrl(callbackUrl);
  Paytm.MerchantProperties.initialize(environment, mid, key, website);
  var channelId = Paytm.EChannelId.WEB;
  var orderId = orderID;
  var txnAmount = Paytm.Money.constructWithCurrencyAndValue(
    Paytm.EnumCurrency.INR,
    sessionPrice.toString()
  );
  var userInfo = new Paytm.UserInfo(studentId);
  userInfo.setAddress("CUSTOMER_ADDRESS");
  userInfo.setEmail(studentEmail);
  userInfo.setFirstName("CUSTOMER_FIRST_NAME");
  userInfo.setLastName("CUSTOMER_LAST_NAME");
  userInfo.setMobile(studentPhone);
  // userInfo.setPincode("CUSTOMER_PINCODE");
  var paymentDetailBuilder = new Paytm.PaymentDetailBuilder(
    channelId,
    orderId,
    txnAmount,
    userInfo
  );
  var paymentDetail = paymentDetailBuilder.build();
  var response = await Paytm.Payment.createTxnToken(paymentDetail);

  console.log("Payment response is " + JSON.stringify(response));
  // console.log("Order is for payment", orderID);

  var paytmParams = {
    MID: process.env.MERCHANT_ID,
    WEBSITE: process.env.WEBSITE,
    INDUSTRY_TYPE_ID: process.env.INDUSTRY_TYPE,
    CHANNEL_ID: process.env.CHANNEL_ID,
    ORDER_ID: orderID,
    CUST_ID: studentId,
    MOBILE_NO: studentPhone,
    EMAIL: studentEmail,
    TXN_AMOUNT: sessionPrice,
    CALLBACK_URL: process.env.CALLBACK_URL,
  };

  return {
    ...paytmParams,
    CHECKSUMHASH: response.responseObject.head.signature,
    TXN_TOKEN: response.responseObject.body.txnToken,
  };
};
export default initiatePayment;
