import axios, {AxiosInstance} from "axios";
export type ListUsersInOrganizationResponse = {
    collection: ReadonlyArray<{
        user: {
            uri: string;
            scheduling_url: string;
        }
    }>;
    pagination: unknown;
}
export type InviteUserToOrganizationResponse = {
    resource: {
        created_at: string;
        email: string;
        last_sent_at: string;
        organization: string;
        status: string;
        updated_at: string;
        uri: string;
    }
}
export type GetScheduledEventsResponse = {
    collection: unknown;
    pagination: unknown;
}
export type OauthTokenResponse = {
    token_type: string,
    expires_in: number,
    created_at: number,
    refresh_token: string,
    access_token: string,
    scope: string,
    owner: string,
    organization: string
}
const organizationUri = "https://api.calendly.com/organizations/CCACRLFBYFAPPJ4V";
const organizationUUID = "CCACRLFBYFAPPJ4V";
export type ErrorResponse = {
    title: string;
    message: string;
}
let refreshToken = "l45HiiucJFh_WIHLxXXNSuUgz7i-Y5nD7nvfrww1I_E";
let accessToken = "gX0MmhG6NE-jJp3ifQSpUvKCES8ENtxNG-J8mmnNVH8";
export default class CalendlyRestClient {
    private restClient: AxiosInstance;
    constructor(baseURL: string) {
        this.restClient = axios.create({
            baseURL: baseURL,
            timeout: 1000 * 60,
        });
    }
    async getOauthToken(): Promise<OauthTokenResponse|ErrorResponse>{
        let baseUrl = "/oauth/token";
        try {
            const response = await this.restClient.post<OauthTokenResponse>(baseUrl, {
                grant_type: "refresh_token",
                client_id: "PI9VVoPhuOTSVlTKxi1T8c8ASuCnNI5G5exCVb3SJ48",
                client_secret: "mziXhlYa62_3xx-grnaOamq51Oktn-V8OzmOvqIiLYQ",
                refresh_token: refreshToken,
            }, {
                headers: {
                    "Content-Type": 'application/json'
                }
            });
            accessToken = response.data.access_token;
            refreshToken = response.data.refresh_token;
            console.log(response.data);
            return response.data as OauthTokenResponse;
        }catch (err) {
            console.log(JSON.stringify(err));
            return {
                title: "ERROR",
                message: JSON.stringify(err),
            }
            throw err;
        }
    }
    async listUsersInOrganizations(email?: string): Promise<ListUsersInOrganizationResponse>{
        let baseUrl = "/organization_memberships/";
        try {
            let params: Object = {
                count: 100,
                organization: organizationUri,
            }
            if(email)params = {...params, email: email};
            const response = await this.restClient.get<ListUsersInOrganizationResponse>(baseUrl, {
                params: params,
                headers: {
                    Authorization: 'Bearer ' + accessToken
                }
            });
            console.log(JSON.stringify(response.data));
            return response.data as ListUsersInOrganizationResponse;
        }catch (err) {
            console.log(JSON.stringify(err));
            throw err;
        }
    }
    async inviteUserToOrganization(email: string): Promise<InviteUserToOrganizationResponse>{
        let baseUrl = "/organizations/"+ organizationUUID + "/invitations";
        try {
            const response = await this.restClient.post<InviteUserToOrganizationResponse>(baseUrl, {
                email: email
            }, {
                headers: {
                    Authorization: 'Bearer ' + accessToken
                }
            });
            console.log(response.data);
            return response.data as InviteUserToOrganizationResponse;
        }catch (err) {
            console.log(JSON.stringify(err));
            throw err;
        }
    }
    async getScheduledEvents(): Promise<GetScheduledEventsResponse|ErrorResponse>{
        let baseUrl = "/scheduled_events/";
        try {
            const response = await this.restClient.get<GetScheduledEventsResponse>(baseUrl, {
                params: {
                    count: 100,
                    organization: organizationUri
                },
                headers: {
                    Authorization: 'Bearer ' + accessToken
                }
            });
            console.log(response.data);
            return response.data as GetScheduledEventsResponse;
        }catch (err) {
            console.log(JSON.stringify(err));
            return {
                title: "ERROR",
                message: JSON.stringify(err),
            }
            throw err;
        }
    }
}
export const devInstance = new CalendlyRestClient("https://api.calendly.com");
// const inviteUserToOrganization = async () => await devInstance.inviteUserToOrganization("xyz@xyz.com");
// inviteUserToOrganization().then(() => console.log('Done!!!'));
// const listUsersInOrganizations = async () => await devInstance.listUsersInOrganizations();
// listUsersInOrganizations().then(() => console.log('Done!!!'));
const getScheduledEvents = async () => await devInstance.getScheduledEvents();
getScheduledEvents().then(() => console.log('Done!!!'));

// const authInstance = new CalendlyRestClient("https://auth.calendly.com");
// const generateAccessAndRefreshToken = async () => await authInstance.getOauthToken();
// generateAccessAndRefreshToken().then(r => console.log('Done ' + JSON.stringify(r)));