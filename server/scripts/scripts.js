"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var MongoClient = require("mongodb").MongoClient;
var chance_1 = require("chance");
var chance = new chance_1.Chance();
function main() {
    return __awaiter(this, void 0, void 0, function () {
        var MONGO_URL, client, i, gender, result, e_1;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 6, , 7]);
                    MONGO_URL = "mongodb+srv://db-admin:N1prvqqiIbZmLvyw@cluster0.ei7rf.mongodb.net/grapes-db?retryWrites=true&w=majority";
                    client = new MongoClient(MONGO_URL);
                    return [4 /*yield*/, client.connect()];
                case 1:
                    _a.sent();
                    i = 0;
                    _a.label = 2;
                case 2:
                    if (!(i < 50)) return [3 /*break*/, 5];
                    gender = i % 2 == 0 ? "female" : "male";
                    return [4 /*yield*/, client
                            .db("grapes-db")
                            .collection("examexperts")
                            .insertOne({
                            name: chance.name(),
                            email: chance.email(),
                            password: "$2b$12$ysBM9lLciIjmTnqFPwRrw.vHjcN8LoyyjCTO0RvZGM2MmLgRwmyTK",
                            phone: "8976789878",
                            profilePic: "https://joeschmoe.io/api/v1/" +
                                gender +
                                "/grapes-" +
                                chance.word(),
                            sessionPrice: 750,
                            schedulingUrl: "https://zoom.us/j/2720386535?pwd=YWNQWW5JdkRaTVMwQ293ZCs1dmplZz09",
                            linkedinUrl: "https://www.linkedin.com/in/aneesh-epari-2b936b123/",
                            domainInfoMap: {
                                CAT: {
                                    "Undergrad Stream": "Commerce",
                                    "CAT percentile": "99.99",
                                    "Post Graduation College": "IIM Ahmedadbad",
                                    "LRDI Strength": chance.d4(),
                                    "English Strength": chance.d4(),
                                    "Work Experience": chance.d4(),
                                    "Quant Strength": chance.d4(),
                                    Attempts: 1
                                },
                                selectedDomain: "CAT",
                                Gender: gender
                            },
                            timestamp: Date.now()
                        })];
                case 3:
                    result = _a.sent();
                    console.log("New listing created with the following id: " + result.insertedId);
                    _a.label = 4;
                case 4:
                    i++;
                    return [3 /*break*/, 2];
                case 5: return [3 /*break*/, 7];
                case 6:
                    e_1 = _a.sent();
                    console.log(e_1);
                    return [3 /*break*/, 7];
                case 7: return [2 /*return*/];
            }
        });
    });
}
main()["catch"](console.error);
// const { MongoClient } = require("mongodb");
//
// async function main() {
//   try {
//     const MONGO_URL = `mongodb+srv://db-admin:N1prvqqiIbZmLvyw@cluster0.ei7rf.mongodb.net/grapes-db?retryWrites=true&w=majority`;
//     const client = new MongoClient(MONGO_URL);
//     await client.connect();
//     for (let i = 0; i < 50; i++) {
//       const result = await client
//         .db("grapes-db")
//         .collection("sessions")
//         .insertOne({
//           examExpertId: "604cd627675851936b2dbb57",
//           studentId: "604ccf071dab3ed72473ffa7",
//           agenda: "Motivation",
//           videoURL: ":(",
//           domain: "IIT",
//           meetingURL: "meet.com/2e2da3e14",
//           startTimestamp: ((i) => {
//             const timeStamp = 1615798526 + i * 15 * 60 * 1000;
//             return timeStamp;
//           })(i),
//           sessionPrice: 150,
//         });
//       console.log(
//         `New listing created with the following id: ${result.insertedId}`
//       );
//     }
//   } catch (e) {
//     console.log(e);
//   }
// }
// main().catch(console.error);
// //for cat and iit exam experts data
// // .collection("examexperts")
// //     .insertOne({
// //         name: "test_iit" + i,
// //         email: "test_iit" + i + "@gmail.com",
// //         password: "test_iit",
// //         phone: "1234567890",
// //         sessionPrice: 250, //default
// //         calendlyUri: "cronofy",
// //         schedulingUrl: "none",
// //         domainInfoMap: {
// //             selectedDomain: "IIT",
// //             Gender: i % 2 ? "Male" : "Female",
// //             IIT: {
// //                 "Maths Strength": 4,
// //                 "Chemistry Strength": 3,
// //                 "Physics Strength": 5,
// //                 College: "IITB",
// //                 "JEE Mains AIR": i,
// //                 "JEE Advanced AIR": i * 10,
// //             },
// //         },
// //     });
// // {
// //     "_id": {
// //     "$oid": "604b63b3fae4db6a07c5315a"
// // },
// //     "name": "test1",
// //     "email": "test1@gmail.com",
// //     "password": "test",
// //     "phone": "1234567890",
// //     "pricePerSession": {
// //     "$numberInt": "100"
// // },
// //     "calendlyUri": "madarchod calendly",
// //     "schedulingUrl": "kuch nahi",
// //     "domainInfoMap": {
// //     "selectedDomain": "IIT",
// //         "Gender": "Male",
// //         "IIT": {
// //         "JEE Advanced AIR": {
// //             "$numberInt": "1"
// //         },
// //         "JEE Mains AIR": {
// //             "$numberInt": "1"
// //         },
// //         "College": "IITB",
// //             "Physics Strength": "4",
// //             "Chemistry Strength": "5",
// //             "Maths Strength": "5"
// //     }
// // }
// // }
//
// // CAT Applicant
// // .insertOne({
// //     name: "test_cat" + i,
// //     email: "test_cat" + i + "@gmail.com",
// //     password: "test_cat",
// //     phone: "1234567890",
// //     sessionPrice: 150, //default
// //     calendlyUri: "madarchod calendly",
// //     schedulingUrl: "kuch nahi",
// //     domainInfoMap: {
// //         selectedDomain: "CAT",
// //         Gender: i % 2 ? "Male" : "Female",
// //         CAT: {
// //             "CAT percentile": 100 - i,
// //             "Undergrad stream": "Science",
// //             College: "IIMA",
// //             "LRDI Strength": "5",
// //             "English Strength": "5",
// //             "Quant Strength": "5",
// //             "Work Experience": 2,
// //         },
// //     },
// // });
//for cat and iit exam experts data
// .collection("examexperts")
//     .insertOne({
//         name: "test_iit" + i,
//         email: "test_iit" + i + "@gmail.com",
//         password: "test_iit",
//         phone: "1234567890",
//         sessionPrice: 250, //default
//         calendlyUri: "cronofy",
//         schedulingUrl: "none",
//         domainInfoMap: {
//             selectedDomain: "IIT",
//             Gender: i % 2 ? "Male" : "Female",
//             IIT: {
//                 "Maths Strength": 4,
//                 "Chemistry Strength": 3,
//                 "Physics Strength": 5,
//                 College: "IITB",
//                 "JEE Mains AIR": i,
//                 "JEE Advanced AIR": i * 10,
//             },
//         },
//     });
// {
//     "_id": {
//     "$oid": "604b63b3fae4db6a07c5315a"
// },
//     "name": "test1",
//     "email": "test1@gmail.com",
//     "password": "test",
//     "phone": "1234567890",
//     "pricePerSession": {
//     "$numberInt": "100"
// },
//     "calendlyUri": "madarchod calendly",
//     "schedulingUrl": "kuch nahi",
//     "domainInfoMap": {
//     "selectedDomain": "IIT",
//         "Gender": "Male",
//         "IIT": {
//         "JEE Advanced AIR": {
//             "$numberInt": "1"
//         },
//         "JEE Mains AIR": {
//             "$numberInt": "1"
//         },
//         "College": "IITB",
//             "Physics Strength": "4",
//             "Chemistry Strength": "5",
//             "Maths Strength": "5"
//     }
// }
// }
// CAT Applicant
// .insertOne({
//     name: "test_cat" + i,
//     email: "test_cat" + i + "@gmail.com",
//     password: "test_cat",
//     phone: "1234567890",
//     sessionPrice: 150, //default
//     calendlyUri: "madarchod calendly",
//     schedulingUrl: "kuch nahi",
//     domainInfoMap: {
//         selectedDomain: "CAT",
//         Gender: i % 2 ? "Male" : "Female",
//         CAT: {
//             "CAT percentile": 100 - i,
//             "Undergrad stream": "Science",
//             College: "IIMA",
//             "LRDI Strength": "5",
//             "English Strength": "5",
//             "Quant Strength": "5",
//             "Work Experience": 2,
//         },
//     },
// });
