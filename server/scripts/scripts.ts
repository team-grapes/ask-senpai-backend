import {createHmac} from "crypto";

const { MongoClient } = require("mongodb");
import { Chance } from "chance";
let chance = new Chance();
async function main() {
  try {
    const MONGO_URL = `mongodb+srv://db-admin:N1prvqqiIbZmLvyw@cluster0.ei7rf.mongodb.net/grapes-db?retryWrites=true&w=majority`;
    const client = new MongoClient(MONGO_URL);
    await client.connect();
    for (let i = 0; i < 50; i++) {
      const gender = i % 2 == 0 ? "female" : "male";
      const result = await client
        .db("grapes-db")
        .collection("examexperts")
        .insertOne({
          name: chance.name(),
          email: chance.email(),
          password:
            "$2b$12$ysBM9lLciIjmTnqFPwRrw.vHjcN8LoyyjCTO0RvZGM2MmLgRwmyTK",
          phone: "8976789878",
          profilePic:
            "https://joeschmoe.io/api/v1/" +
            gender +
            "/grapes-" +
            chance.word(),
          sessionPrice: 750,
          schedulingUrl:
            "https://zoom.us/j/2720386535?pwd=YWNQWW5JdkRaTVMwQ293ZCs1dmplZz09",
          linkedinUrl: "https://www.linkedin.com/in/aneesh-epari-2b936b123/",
          domainInfoMap: {
            CAT: {
              "Undergrad Stream": "Commerce",
              "CAT percentile": "99.99",
              "Post Graduation College": "IIM Ahmedadbad",
              "LRDI Strength": chance.d4(),
              "English Strength": chance.d4(),
               "Work Experience" : chance.d4(),
              "Quant Strength": chance.d4(),
              Attempts: 1,
            },
            selectedDomain: "CAT",
            Gender: gender,
          },
          timestamp : Date.now()
        });
      console.log(
        `New listing created with the following id: ${result.insertedId}`
      );
    }
  } catch (e) {
    console.log(e);
  }
}
main().catch(console.error);
// const { MongoClient } = require("mongodb");
//
// async function main() {
//   try {
//     const MONGO_URL = `mongodb+srv://db-admin:N1prvqqiIbZmLvyw@cluster0.ei7rf.mongodb.net/grapes-db?retryWrites=true&w=majority`;
//     const client = new MongoClient(MONGO_URL);
//     await client.connect();
//     for (let i = 0; i < 50; i++) {
//       const result = await client
//         .db("grapes-db")
//         .collection("sessions")
//         .insertOne({
//           examExpertId: "604cd627675851936b2dbb57",
//           studentId: "604ccf071dab3ed72473ffa7",
//           agenda: "Motivation",
//           videoURL: ":(",
//           domain: "IIT",
//           meetingURL: "meet.com/2e2da3e14",
//           startTimestamp: ((i) => {
//             const timeStamp = 1615798526 + i * 15 * 60 * 1000;
//             return timeStamp;
//           })(i),
//           sessionPrice: 150,
//         });
//       console.log(
//         `New listing created with the following id: ${result.insertedId}`
//       );
//     }
//   } catch (e) {
//     console.log(e);
//   }
// }
// main().catch(console.error);
// //for cat and iit exam experts data
// // .collection("examexperts")
// //     .insertOne({
// //         name: "test_iit" + i,
// //         email: "test_iit" + i + "@gmail.com",
// //         password: "test_iit",
// //         phone: "1234567890",
// //         sessionPrice: 250, //default
// //         calendlyUri: "cronofy",
// //         schedulingUrl: "none",
// //         domainInfoMap: {
// //             selectedDomain: "IIT",
// //             Gender: i % 2 ? "Male" : "Female",
// //             IIT: {
// //                 "Maths Strength": 4,
// //                 "Chemistry Strength": 3,
// //                 "Physics Strength": 5,
// //                 College: "IITB",
// //                 "JEE Mains AIR": i,
// //                 "JEE Advanced AIR": i * 10,
// //             },
// //         },
// //     });
// // {
// //     "_id": {
// //     "$oid": "604b63b3fae4db6a07c5315a"
// // },
// //     "name": "test1",
// //     "email": "test1@gmail.com",
// //     "password": "test",
// //     "phone": "1234567890",
// //     "pricePerSession": {
// //     "$numberInt": "100"
// // },
// //     "calendlyUri": "madarchod calendly",
// //     "schedulingUrl": "kuch nahi",
// //     "domainInfoMap": {
// //     "selectedDomain": "IIT",
// //         "Gender": "Male",
// //         "IIT": {
// //         "JEE Advanced AIR": {
// //             "$numberInt": "1"
// //         },
// //         "JEE Mains AIR": {
// //             "$numberInt": "1"
// //         },
// //         "College": "IITB",
// //             "Physics Strength": "4",
// //             "Chemistry Strength": "5",
// //             "Maths Strength": "5"
// //     }
// // }
// // }
//
// // CAT Applicant
// // .insertOne({
// //     name: "test_cat" + i,
// //     email: "test_cat" + i + "@gmail.com",
// //     password: "test_cat",
// //     phone: "1234567890",
// //     sessionPrice: 150, //default
// //     calendlyUri: "madarchod calendly",
// //     schedulingUrl: "kuch nahi",
// //     domainInfoMap: {
// //         selectedDomain: "CAT",
// //         Gender: i % 2 ? "Male" : "Female",
// //         CAT: {
// //             "CAT percentile": 100 - i,
// //             "Undergrad stream": "Science",
// //             College: "IIMA",
// //             "LRDI Strength": "5",
// //             "English Strength": "5",
// //             "Quant Strength": "5",
// //             "Work Experience": 2,
// //         },
// //     },
// // });

//for cat and iit exam experts data
// .collection("examexperts")
//     .insertOne({
//         name: "test_iit" + i,
//         email: "test_iit" + i + "@gmail.com",
//         password: "test_iit",
//         phone: "1234567890",
//         sessionPrice: 250, //default
//         calendlyUri: "cronofy",
//         schedulingUrl: "none",
//         domainInfoMap: {
//             selectedDomain: "IIT",
//             Gender: i % 2 ? "Male" : "Female",
//             IIT: {
//                 "Maths Strength": 4,
//                 "Chemistry Strength": 3,
//                 "Physics Strength": 5,
//                 College: "IITB",
//                 "JEE Mains AIR": i,
//                 "JEE Advanced AIR": i * 10,
//             },
//         },
//     });
// {
//     "_id": {
//     "$oid": "604b63b3fae4db6a07c5315a"
// },
//     "name": "test1",
//     "email": "test1@gmail.com",
//     "password": "test",
//     "phone": "1234567890",
//     "pricePerSession": {
//     "$numberInt": "100"
// },
//     "calendlyUri": "madarchod calendly",
//     "schedulingUrl": "kuch nahi",
//     "domainInfoMap": {
//     "selectedDomain": "IIT",
//         "Gender": "Male",
//         "IIT": {
//         "JEE Advanced AIR": {
//             "$numberInt": "1"
//         },
//         "JEE Mains AIR": {
//             "$numberInt": "1"
//         },
//         "College": "IITB",
//             "Physics Strength": "4",
//             "Chemistry Strength": "5",
//             "Maths Strength": "5"
//     }
// }
// }
// CAT Applicant
// .insertOne({
//     name: "test_cat" + i,
//     email: "test_cat" + i + "@gmail.com",
//     password: "test_cat",
//     phone: "1234567890",
//     sessionPrice: 150, //default
//     calendlyUri: "madarchod calendly",
//     schedulingUrl: "kuch nahi",
//     domainInfoMap: {
//         selectedDomain: "CAT",
//         Gender: i % 2 ? "Male" : "Female",
//         CAT: {
//             "CAT percentile": 100 - i,
//             "Undergrad stream": "Science",
//             College: "IIMA",
//             "LRDI Strength": "5",
//             "English Strength": "5",
//             "Quant Strength": "5",
//             "Work Experience": 2,
//         },
//     },
// });
